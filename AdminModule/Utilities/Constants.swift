//
//  Constants.swift
//  Sale AU
//
//  Created by Talentelgia on 9/19/17.
//  Copyright © 2017 Talentelgia. All rights reserved.
//

import Foundation
import UIKit

//MARK:***************API List******************

let KBaseUrl              = "http://Example.com"
let KImageBaseUrl         = ""
let KVideoBaseUrl         = ""
let KCompanyImageUrl      = ""
let KLoginUrl             = "/login"
let KForgotPwdUrl         = "/forgotpassword"
let KBusinessOwnerList    = "/businessowner/mobile"
let KResetPwdUrl          = "/login/resetpassword"
let KPaymentsUrl          = "/payments/mobile"
let KChangePwdUrl         = "/authRoutes/changePassword"
let KGetpaymentsBydateUrl  = "/payment/getpaymentbydaterange/mobile"
let KGetBusinessOwnersByDate = "/businessowners/getownersbydaterange/mobile"
let KGetprofile            = "/user/profile"
let KUpdateProfile       = "/user/updateUser"
let KDashBoardData       = "/dashboard/getcountsforsuperadmin"
let KUpdateCompany       = "/businessowners/updateCompany"
let KDeleteMultipleUsers = "/commonServices/deletemultirecords/"
let KChangeStatusUsers   = "/commonServices/updatemultistatus/"
let KDeleteSingleUser    = "/businessowner/"
let KUpdateSingleUserStatus = "/businessowners/updatestatus/"
let KAddBusinessUser      = "/businessowners/addbusinessowner"





//MARK:*************** Keys **********************

let KEmailKey          = "email"
let KPwdKey            = "password"
let KNewPwdKey         = "new_password"
let KOldPwdKey         = "old_password"
let KConfPwdKey        = "confirm_password"
let KRecoverCodeKey    = "recovery_code"
let KFullNameKey       = "fullname"
let KStartdateKey      = "start_date"
let KEnddateKey        = "end_date"



//MARK:*************** Storyboard Identifier******************

let KAgeConfirmVC                   =  "sid_AgeConfirmVC"
let kLoginVC                        =  "sid_LoginVC"
let kSignUpVC                       =  "sid_SignUpVC"
let kForgotPwdVC                    =  "sid_ForgotPwdVC"
let kSWRevealVC                     =  "sid_SWRevealVC"
let KSignUpTermsVC                  =  "sid_SignUpTermsVC"
let KForgotPwdOtpVC                 =  "sid_ForgotPwdOtpVC"
let KDashBoardVC                    =  "sid_DashBoardVC"
let KStrainDetailsVC                =  "sid_StrainDetailsVC"
let KCurrentOrdersVC                =  "sid_CurrentOrdersVC"
let KPaymentVC                      =  "sid_PaymentVC"
let KPaymentConfirmationVC          =  "sid_PaymentConfirmationVC"
let KFlightPathVC                   =  "sid_FlightPathVC"
let KElevationVC                    =  "sid_ElevationVC"
let KLaunchVC                       =  "sid_LaunchVC"
let KAboutUsVC                      =  "sid_AboutUsVC"
let KPrivacyPolicyVC                =  "sid_PrivacyPolicyVC"
let KTermsOfUseVC                   =  "sid_TermsOfUseVC"
let KFAQsVC                         =  "sid_FAQsVC"
let KManageProfileVC                =  "sid_ManageProfileVC"
let KShippingDetailsVC              =  "sid_ShippingDetailsVC"
let KOrderedHistoryVC               =  "sid_OrderedHistoryVC"
let KFavouriteProductsVC            =  "sid_FavouriteProductsVC"
let KLegalMenuVC                    =  "sid_LegalVC"
let KCategoryVC                     =  "sid_CategoryVC"
let KStrainTypeVC                   =  "sid_StrainTypesVC"
let KMedicalListVC                  =  "sid_FlightPathMedicalVC"
let KRecreationalVC                 =  "sid_FlightPathRecreationalList"



//MARK:*************** Alert Messages **********************

let kAllFields_Alert                =  "Please enter all fields."
let kEmail_Alert                    =  "Please enter an email id."
let kValidemail_Alert               =  "Please enter valid email."
let kPassword_Alert                 =  "Please enter password."
let kUploadImg_Alert                =  "Please upload image."
let KPasswordLimitAlert            =  "Password should be 4 to 8 characters."
let kOldPwdAlert                   =  "Please enter old password."
let kNewPwdAlert                   =  "Please enter new password."
let kConfirmPwdAlert               =  "Please enter confirm password."
let KPwdNotMatchAlert              =  "Password and confirm password does not match."
let KPwdSameAlert              =  "Password can not be same as previous password."
let KNewPwdNotMatchAlert           =  "New password and confirm password does not match."
let KUpdatedDataAlert              =  "Profile updated successfully."
let KAddedDataAlert                =  "User added successfully."
let KAddBusinessUser_Alert         =  "Business owner added successfully."

let KUdatedPasswordAlert           =  "Password updated successfully."
let kFirstName_Alert               =  "Please enter first name."
let klastName_Alert                =  "Please enter last name."
let kPlanName_Alert                =  "Please enter plan name."
let kCompanyName_Alert             =  "Please enter company name."
let kPhoneNumber_Alert             =  "Please enter phone number."
let kWebsitelink_Alert             =  "Please enter website link."
let kAddress_Alert                 =  "Please enter Address."
let kCity_Alert                    =  "Please enter city name."
let kState_Alert                   =  "Please enter state name."
let kPostalCode_Alert              =  "Please enter postal code."
let kStripePublicKey_Alert         =  "Please enter stripe public key."
let kStripePrivateKey_Alert        =  "Please enter stripe private key."
let kQBCLientId_Alert              =  "Please enter QB client id."
let kQBCLientSecret_Alert          =  "Please enter QB client secret."
let kQBCLientCompanyID_Alert       =  "Please enter QB company id."
let kUserDeleted                   =  "User deleted successfully."
let kSelectedAction                =  "Please select one option."
let kSelectCheckBox                =  "Please select atleast one checkbox."
let kStatusUpdated               =  "Status updated successfully."
let kStartDateAlert               =  "Please enter start date."
let kEndDateDateAlert               =  "Please enter end date."
let kNointernetAlert               =  "Your not connected to internet.Please check internet connection."
let KMaxDateAlert               =  "End date should be greater than start date."
let KCompanyNameAlert           = "Please enter company name."
let KPlanUpdatedAlert           =  "Plan updated successfully."
let KPlanNameAlert              =  "Please enter plan name."
let KPlanAddedAlert             =  "Plan created successfully."
let KStatus_Alert               =  "Please select status."
let KPlanid_Alert               =  "Please enter plan Id"
let KValidity_Alert             =  "Please enter validity."
let KValidityInterval_Alert     =  "Please select validity interval."
let KDescription_Alert          =  "Please enter description."
let KCurrency_Alert             =  "Please enter currency."
let KPrice_Alert                =  "Please enter price."
let KPlanDeletedAlert         = "Coupon deleted successfully."
let KCouponCodeAlert          = "Coupon code should not be empty."
let KCouponDescriptionAlert          = "Coupon description  should not be empty."
let KCouponValueAlert         = "Coupon value  should not be empty."
let KCouponMaxRedemptionsAlert         = "Coupon max redemptions  should not be empty."
let KCouponStartDateAlert         = "Start date should not be empty."
let KCouponEndDateAlert         = "End date should not be empty."
let KCouponTypeAlert         = "Coupon type should not be empty."
let KCouponCurrencyAlert         = "Currency should not be empty."
let KCouponDurationAlert         = "Duration should not be empty."
let KCouponStatusAlert         = "Status should not be empty."
let KCouponAddAlert         = "Coupon added successfully."
let KPageTitle         = "Page title should not be empty."
let KPageMetatag        = "Page meta tag should not be empty."
let KPageUpdatedAlert           =  "Page content has been updated successfully."
let KPasswordEmpty      = "Password should not be empty."
let KUserRoleEmpty      = "Please select user role."
//MARK:*************** Cell Identifiers **********************

enum CellIdentifier : String {
    case KDashBoardCustomCellIdentifier            = "DashBoardCustomCellIdentifier"
    case KCurrentOrdersCustomCellIdentifier        = "CurrentOrdersCustomCellIdentifier"
    case KOrdereHistoryCustomCellIdentifier       = "OrdereHistoryCustomCellIdentifier"
    case KReviewsCustomCell                        = "ReviewsCustomCellIdentifier"
    case KSlideMenuCellIdentifier                  = "SlideMenuCellIdentifier"
    case KLegalMenuCellIdentifier                  = "LegalCellIdentifier"
    case KFilterCellIdentifier                    = "FilterCollectionCell"
}


//MARK:- Set Bool value for RememberMe
func rememberme(rememberme : Bool)->Void {
    UserDefaults.standard.set(rememberme, forKey: "Rememberme");    UserDefaults.standard.synchronize()
}
//MARK:- Get Bool value for RememberMei
func getRememberme() -> Bool {
    return UserDefaults.standard.bool(forKey: "Rememberme")
}

//MARK:- Set RootView value for RootView
func setRootView(selectedme : Bool)->Void {
    UserDefaults.standard.set(selectedme, forKey: "RootViewme");    UserDefaults.standard.synchronize()
}
//MARK:- Get RootView value for RootView
func getRootView() -> Bool {
    return UserDefaults.standard.bool(forKey: "RootViewme")
}

//MARK:- Save Credit card details.
func saveCreditCard(creditDict :[String:String])->Void {
    UserDefaults.standard.set(creditDict, forKey: "creditCard");    UserDefaults.standard.synchronize()
}
//MARK:- Get Credit card details.
func getCreditCard() -> [String:String] {
    return UserDefaults.standard.value(forKey: "creditCard") as? [String : String] ?? [:]
}


//MARK:- Check the image Url is valid or not.
func isValid(urlString: String) -> Bool
{
    var boolValue:Bool = false
    let dataUrl = NSURL(string: urlString)
    if dataUrl != nil{
        boolValue =  true
    }else{
        boolValue =  false
    }
    return boolValue
}

//MARK:- App link url
let KAppId = "1256995866"
let kiOSUrl  = "https://itunes.apple.com/us/app/little-picasso/id1256995866?ls=1&mt=8"

//MARK:- Application Title.

let KAPP_NAME = "ROOFMATE"
let kCancelTitle = "Ok"


//MARK:*************** Date formats  **********************

let KInputformat         = "yyyy-MM-dd HH:mm:ss"
let KOutputformat1       = "dd/MM/yyyy"
let KOutputformat        = "yyyy-MM-dd"
let kInputDateformat     = "MMM dd,yyyy"
let KTimeformat          = "hh:mm a"
let KcalenderFormat      = "yyyy-MM-dd"
let KNotifyFormat        = "yyyy-MM-dd,hh:mm a"
let kCommentFormat       = "dd MMM yyyy, hh:mm a"


//MARK:- Convert HexString to UIColor.
func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
var color1 = hexStringToUIColor(hex: "#4e9192")
var color2 = hexStringToUIColor(hex: "#84c3a2")


class GradientView_90: UIView {
    override open class var layerClass: AnyClass {
        get{
            return CAGradientLayer.classForCoder()
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = self.layer as! CAGradientLayer
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradientLayer.colors = [color1.cgColor,color2.cgColor]
    }
}

class GradientView: UIView {
    override open class var layerClass: AnyClass {
        get{
            return CAGradientLayer.classForCoder()
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = self.layer as! CAGradientLayer
        gradientLayer.colors = [color1.cgColor,color2.cgColor]
    }
}

class DetailsGradientView: UIView {
    override open class var layerClass: AnyClass {
        get{
            return CAGradientLayer.classForCoder()
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = self.layer as! CAGradientLayer
        gradientLayer.colors = [color3.cgColor,color4.cgColor]
    }
}
var color3 = hexStringToUIColor(hex: "#FE9F8C")
var color4 = hexStringToUIColor(hex: "#FFB884")


class GradientNavBar_90: UINavigationBar {
    override open class var layerClass: AnyClass {
        get{
            return CAGradientLayer.classForCoder()
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = self.layer as! CAGradientLayer
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.2)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.2)
        gradientLayer.colors = [color1.cgColor,color2.cgColor]
    }
}

class GradientNavBar: UINavigationBar {
    override open class var layerClass: AnyClass {
        get{
            return CAGradientLayer.classForCoder()
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = self.layer as! CAGradientLayer
        gradientLayer.colors = [color1.cgColor,color2.cgColor]
    }
}


//MARK:- Device screen sizes
struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}
//MARK:- Device screen types
struct DeviceType {
    static let IS_IPHONE_4  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
    
}

//MARK:- Instance for AppDelegate
let appDelegate = UIApplication.shared.delegate as! AppDelegate


func imageLayerForGradientBackground(navigationBar:UINavigationBar) -> UIImage {
    var updatedFrame = navigationBar.bounds
    // take into account the status bar
    updatedFrame.size.height += 20
    let layer = CAGradientLayer.gradientLayerForBounds(bounds: updatedFrame)
    UIGraphicsBeginImageContext(layer.bounds.size)
    layer.render(in: UIGraphicsGetCurrentContext()!)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image!
}

extension CAGradientLayer {
    class func gradientLayerForBounds(bounds: CGRect) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = bounds
        layer.colors = [color2.cgColor,color1.cgColor]
        return layer
    }
}

let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

let maxNumberOfCharacters = 16

