//
//  ServiceParser.swift
//  Crown Gospel
//
//  Created by mac min  on 28/12/17.
//  Copyright © 2017 mac min . All rights reserved.
//

import Foundation
import UIKit

class Parser{
    
    
    static func userJsonToModel(dict: [String:Any])->UserModel{
        let userData:UserModel = UserModel()
            if let email = dict["email"] as? String{
                userData.email = email
            }
            if let name = dict["first_name"] as? String{
                userData.first_name = name
            }
            if dict["id"] is String {
                if let user_id = dict["id"] as? String{
                    userData.user_id = user_id
                }
            }
            else{
                if let user_id = dict["id"] as? Int{
                    userData.user_id = String(user_id)
                }
            }
        
        if dict["plan_id"] is String {
            if let user_id = dict["plan_id"] as? String{
                userData.plan_id = user_id
            }
        }
        else{
            if let user_id = dict["plan_id"] as? Int{
                userData.plan_id = String(user_id)
            }
        }
        
            if let dp = dict["profile_image"] as? String{
                userData.imageName = dp
            }
            if let roleId = dict["role_id"] as? String{
                userData.user_type = roleId
            }
            if let lastName = dict["last_name"] as? String{
                userData.last_name = lastName
            }
            if let token = dict["token"] as? String{
                userData.token = token
            }
            if let phoneNumber = dict["phone_number"] as? String{
                userData.phone_number = phoneNumber
            }
           if let plan_purchase_date = dict["plan_purchase_date"] as? String{
            userData.planPurchaseDate = plan_purchase_date
           }
           if let validity = dict["validity"] as? String{
            userData.validity = validity
           }
          if let validity_interval = dict["validity_interval"] as? String{
            userData.validityInterval = validity_interval
          }
            if  dict["status"] is String {
                if let status =  dict["status"] as? String{
                    userData.status = status
                }
            }
            else{
                if let status = dict["status"] as? Int{
                    userData.status = String(status)
                }
            }
            if let slug = dict["slug"] as? String{
                userData.slug = slug
            }
           if let companySlug = dict["company_slug"] as? String{
            userData.companySlug = companySlug
            }
        if let qbo_company_id = dict["qbo_company_id"] as? String{
            userData.qbo_company_id = qbo_company_id
        }
      
        if let company_id = dict["company_id"] as? String{
            userData.company_id = company_id
        }
        return userData
    }
    
    static func ownersJsonToModel(dict: [[String:Any]])->[UserModel]{
        var imageModelArray:[UserModel]=[UserModel]()
        for dataValue in dict{

            let userData:UserModel=UserModel()
            if let email = dataValue["email"] as? String{
                userData.email = email
            }
            if let name = dataValue["first_name"] as? String{
                userData.first_name = name
            }
            if dataValue["id"] is String {
                if let user_id = dataValue["id"] as? String{
                    userData.user_id = user_id
                }
            }
            else{
                if let user_id = dataValue["id"] as? Int{
                    userData.user_id = String(user_id)
                }
            }
            if let dp = dataValue["profile_image"] as? String{
                userData.imageName = dp
            }
            if let roleId = dataValue["role_id"] as? String{
                userData.user_type = roleId
            }
            if let lastName = dataValue["last_name"] as? String{
                userData.last_name = lastName
            }
            if let token = dataValue["token"] as? String{
                userData.token = token
            }
            if let phoneNumber = dataValue["phone_number"] as? String{
                userData.phone_number = phoneNumber
            }
           
            
            if  dataValue["status"] is String {
                if let status =  dataValue["status"] as? String{
                    userData.status = status
                }
            }
            else{
                if let status = dataValue["status"] as? Int{
                    userData.status = String(status)
                }
            }
            
            if let slug = dataValue["slug"] as? String{
                userData.slug = slug
            }
            if let companyInfo = dataValue["company"] as? [String:Any]{
                userData.company = companyJsonToModel(dict: companyInfo)
            }
            if let subscriptionPlan = dataValue["subscription_plan"] as? [String:Any]{
                userData.plan = paymentJsonToModel(dict: subscriptionPlan)
            }
            if let subscriptionPlan = dataValue["role"] as? [String:Any]{
                userData.user_Role = roleJsonToModel(dict: subscriptionPlan)
            }
            
            if let address = dataValue["address"] as? [String:Any]{
                userData.address = addressJsonToModel(dict: address)
            }
            imageModelArray.append(userData)
        }
        return imageModelArray
    }
    
    
    
    static func companyJsonToModel(dict: [String:Any])->CompanyModel{
        let companyData:CompanyModel = CompanyModel()
        if let email = dict["name"] as? String{
            companyData.comName = email
        }
        if dict["id"] is String {
            if let user_id = dict["id"] as? String{
               companyData.compId = user_id
            }
        }
        else{
            if let user_id = dict["id"] as? Int{
                companyData.compId = String(user_id)
            }
        }
        
        if let logo = dict["logo"] as? String{
            companyData.logo = logo
        }
        
        if dict["qbo_client_id"] is String {
            if let qbo_client_id = dict["qbo_client_id"] as? String{
                companyData.qbo_client_id = qbo_client_id
            }
        }
        else{
            if let qbo_client_id = dict["qbo_client_id"] as? Int{
                companyData.qbo_client_id = String(qbo_client_id)
            }
        }
        
        if dict["qbo_client_secret"] is String {
            if let qbo_client_secret = dict["qbo_client_secret"] as? String{
                companyData.qbo_client_secret = qbo_client_secret
            }
        }
        else{
            if let qbo_client_secret = dict["qbo_client_secret"] as? Int{
                companyData.qbo_client_secret = String(qbo_client_secret)
            }
        }
        
        if dict["stripe_private_key"] is String {
            if let stripe_private_key = dict["stripe_private_key"] as? String{
                companyData.stripe_private_key = stripe_private_key
            }
        }
        else{
            if let stripe_private_key = dict["stripe_private_key"] as? Int{
                companyData.stripe_private_key = String(stripe_private_key)
            }
        }
        
        if dict["stripe_public_key"] is String {
            if let stripe_public_key = dict["stripe_public_key"] as? String{
                companyData.stripe_public_key = stripe_public_key
            }
        }
        else{
            if let stripe_private_key = dict["stripe_private_key"] as? Int{
                companyData.stripe_private_key = String(stripe_private_key)
            }
        }
        
        
        if dict["qbo_company_id"] is String {
            if let qbo_company_id = dict["qbo_company_id"] as? String{
                companyData.qbo_company_id = qbo_company_id
            }
        }
        else{
            if let qbo_company_id = dict["qbo_company_id"] as? Int{
                companyData.qbo_company_id = String(qbo_company_id)
            }
        }
        
        return companyData
    }
    
   
    
    static func paymentJsonToModel(dict: [String:Any])->PlanModel{
        
        let planData:PlanModel = PlanModel()
        if let email = dict["name"] as? String{
            planData.planName = email
        }
        if dict["id"] is String {
            if let user_id = dict["id"] as? String{
               planData.planId = user_id
            }
        }
        else{
            if let user_id = dict["id"] as? Int{
                planData.planId = String(user_id)
            }
        }
        if let createdAt = dict["created_at"] as? String{
            planData.createdAt = createdAt
        }
        
        if let currency = dict["currency"] as? String{
            planData.currency = currency
        }
        
        if let description = dict["description"] as? String{
            planData.planDescription = description
        }
        
        if let updated_at = dict["updated_at"] as? String{
            planData.updatedAt = updated_at
        }
        
        if let validity = dict["validity"] as? String{
            planData.validity = validity
        }
        
        if let validity_interval = dict["validity_interval"] as? String{
            planData.validityInterval = validity_interval
        }
        return planData
    }
    
    static func addressJsonToModel(dict: [String:Any])->AddressModel{
        let addressData:AddressModel = AddressModel()
        if let address_one = dict["address_one"] as? String{
            addressData.address_one = address_one
        }
        if let address_two = dict["address_two"] as? String{
            addressData.address_two = address_two
        }
        if dict["id"] is String {
            if let user_id = dict["id"] as? String{
                addressData.id = user_id
            }
        }
        else{
            if let user_id = dict["id"] as? Int{
                addressData.id = String(user_id)
            }
        }
        if let created_at = dict["created_at"] as? String{
            addressData.created_at = created_at
        }
        
        if let city = dict["city"] as? String{
            addressData.city = city
        }
        
        if let link = dict["link"] as? String{
            addressData.link = link
        }
        if dict["postal_code"] is String {
            if let postal_code = dict["postal_code"] as? String{
                addressData.postal_code = postal_code
            }
        }
        else{
            if let postal_code = dict["postal_code"] as? Int{
                addressData.postal_code = String(postal_code)
            }
        }
        if let state = dict["state"] as? String{
            addressData.state = state
        }
        
        if dict["user_id"] is String {
            if let user_id = dict["user_id"] as? String{
                addressData.user_id = user_id
            }
        }
        else{
            if let user_id = dict["user_id"] as? Int{
                addressData.user_id = String(user_id)
            }
        }
        
        return addressData
    }
    
    // Transactions Model Parsing
    
    static func transactionsJsonToModel(Arr: [[String:Any]])->[TransactionsModel]{
        
        var transModelArray:[TransactionsModel]=[TransactionsModel]()
        
        for dataValue in Arr{
            
            let transModel:TransactionsModel=TransactionsModel()
            
            if let id = dataValue["transaction_id"] as? String{
                transModel.transId = id
            }
            if let owner = dataValue["user"] as? NSDictionary {
                let fName = owner.value(forKey: "first_name") as? String ?? ""
                let lName = owner.value(forKey: "last_name") as? String ?? ""
                transModel.ownerName = fName + lName
            }
            if let type = dataValue["payment_type"] as? String{
                transModel.paymentType = type
            }
            if let amount = dataValue["amount"] as? String{
                transModel.amount = amount
            }
            if let date = dataValue["created_at"] as? String{
                transModel.date = date
            }
            transModelArray.append(transModel)
        }
        return transModelArray
    }
    
    
    
    // Payment Model Parsing
    
    static func paymentJsonToModel(Arr: [[String:Any]])->[PaymentsModel]{
        
        var transModelArray:[PaymentsModel]=[PaymentsModel]()
        
        for dataValue in Arr{
            
            let transModel:PaymentsModel = PaymentsModel()
            
            if let id = dataValue["transaction_id"] as? String{
                transModel.transId = id
            }
            if let owner = dataValue["user"] as? NSDictionary {
                let fName = owner.value(forKey: "first_name") as? String ?? ""
                let lName = owner.value(forKey: "last_name") as? String ?? ""
                transModel.ownerName = fName + lName
            }
            if let type = dataValue["payment_method"] as? String{
                transModel.paymentType = type
            }
            if let amount = dataValue["amount"] as? String{
                transModel.amount = amount
            }
            if let date = dataValue["created_at"] as? String{
                transModel.date = date
            }
            transModelArray.append(transModel)
        }
        return transModelArray
    }
    
    
    
    // Pages Model Parsing
    
    static func pagesJsonToModel(Arr: [[String:Any]])->[PagesModel]{
        
        var pagesModelArray:[PagesModel]=[PagesModel]()
        
        for dataValue in Arr{
            let pagesModel:PagesModel=PagesModel()
            
            if dataValue["id"] is String {
                if let id = dataValue["id"] as? String{
                     pagesModel.id = id
                }
            }
            else{
                if let id = dataValue["id"] as? Int{
                     pagesModel.id = String(id)
                }
            }
            
            
            if let title = dataValue["title"] as? String{
                pagesModel.title = title
            }
            if let content = dataValue["content"] as? String{
                pagesModel.content = content
            }
            if let meta_tags = dataValue["meta_tags"] as? String{
                pagesModel.metaTag = meta_tags
            }
            pagesModelArray.append(pagesModel)
        }
        return pagesModelArray
    }
    
    
    
    static func roleJsonToModel(dict: [String:Any])->UserRoleModel{
        let roleData:UserRoleModel = UserRoleModel()
        if let name = dict["name"] as? String{
            roleData.name = name
        }
        if dict["id"] is String {
            if let id = dict["id"] as? String{
                roleData.id = id
            }
        }
        else{
            if let id = dict["id"] as? Int{
                roleData.id = String(id)
            }
        }
        if let created_at = dict["created_at"] as? String{
            roleData.created_at = created_at
        }
        if let updated_at = dict["updated_at"] as? String{
            roleData.updated_at = updated_at
        }
      
        return roleData
      
    }
    
    // Subscription Model Parsing
    
    static func subscriptionJsonToModel(dict: [[String:Any]])->[PlanModel]{
        var planModelArray:[PlanModel]=[PlanModel]()
        for dataValue in dict{
            
            let planData:PlanModel = PlanModel()
            if let email = dataValue["name"] as? String{
                planData.planName = email
            }
            if dataValue["id"] is String {
                if let user_id = dataValue["id"] as? String{
                    planData.planId = user_id
                }
            }
            else{
                if let user_id = dataValue["id"] as? Int{
                    planData.planId = String(user_id)
                }
            }
            if let createdAt = dataValue["created_at"] as? String{
                planData.createdAt = createdAt
            }
            
            if let currency = dataValue["currency"] as? String{
                planData.currency = currency
            }
            
            if let description = dataValue["description"] as? String{
                planData.planDescription = description
            }
            
            if let updated_at = dataValue["updated_at"] as? String{
                planData.updatedAt = updated_at
            }
            
            if let validity = dataValue["validity"] as? String{
                planData.validity = validity
            }
            
            if let validity_interval = dataValue["validity_interval"] as? String{
                planData.validityInterval = validity_interval
            }
            if let slug = dataValue["slug"] as? String{
                planData.slug = slug
            }
            
            if dataValue["price"] is String {
                if let price = dataValue["price"] as? String{
                    planData.price = price
                }
            }
            else{
                if let price = dataValue["price"] as? Int{
                    planData.price = String(price)
                }
            }
            
            if dataValue["status"] is String {
                if let status = dataValue["status"] as? String{
                    planData.status = status
                }
            }
            else{
                if let status = dataValue["status"] as? Int{
                    planData.status = String(status)
                }
            }
            planModelArray.append(planData)
        }
        return planModelArray
    }
    
    
    
    
    // Coupons Model Parsing
    
    static func couponJsonToModel(dict: [[String:Any]])->[CouponModel]{
        var planModelArray:[CouponModel]=[CouponModel]()
        for dataValue in dict{
            
            let planData:CouponModel = CouponModel()
            if let value = dataValue["value"] as? String{
                planData.value = value
            }
            
            if let updated_at = dataValue["updated_at"] as? String{
                planData.updated_at = updated_at
            }
            if let created_at = dataValue["created_at"] as? String{
                planData.created_at = created_at
            }
            if let end_date = dataValue["end_date"] as? String{
                planData.end_date = end_date
            }
            if let start_date = dataValue["start_date"] as? String{
                planData.start_date = start_date
            }
            if let duration = dataValue["duration"] as? String{
                planData.duration = duration
            }
            if let couponDescription = dataValue["description"] as? String{
                planData.couponDescription = couponDescription
            }
            if let type = dataValue["type"] as? String{
                planData.type = type
            }
            if let max_redemptions = dataValue["max_redemptions"] as? String{
                planData.max_redemptions = max_redemptions
            }
            if let created_at = dataValue["created_at"] as? String{
                planData.created_at = created_at
            }
            if let slug = dataValue["slug"] as? String{
                planData.slug = slug
            }
            if let currency = dataValue["currency"] as? String{
                planData.currency = currency
            }
            if dataValue["id"] is String {
                if let id = dataValue["id"] as? String{
                    planData.id = id
                }
            }
            else{
                if let id = dataValue["id"] as? Int{
                    planData.id = String(id)
                }
            }
            
            if dataValue["status"] is String {
                if let status = dataValue["status"] as? String{
                    planData.status = status
                }
            }
            else{
                if let status = dataValue["status"] as? Int{
                    planData.status = String(status)
                }
            }
            
            if dataValue["code"] is String {
                if let code = dataValue["code"] as? String{
                    planData.code = code
                }
            }
            else{
                if let code = dataValue["code"] as? Int{
                    planData.code = String(code)
                }
            }
            
            planModelArray.append(planData)
        }
        return planModelArray
    }
    
    
    
    static func screenShotMethod()->UIImage{
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return screenshot!
    }
}

/*
 
 ["last_login": 2017-09-22 05:54:52, "company_id": 67, "is_staff": 0, "remember_token": kxzO9dRc7C26b9dO, "profile_image": user-1520933058.png, "phone_number": , "last_name": charles, "slug": 6PpuSNKSdDMLiuEWXdoCVg==, "id": 116, "payment_status": 0, "company": {
 "created_at" = "2017-09-22 05:54:52";
 id = 67;
 logo = "company-1511526920.png";
 name = "charles group";
 "plan_id" = 8;
 "plan_purchase_date" = "2018-01-11 10:47:22";
 "qbo_access_token" = "eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..zUYukrSdzQPT7hF7exLOPA.caR9mLwkDeWiNR1Ij2l0C-bzy84AKGqAhDe1Izs0E8hEYq6ZMG1edZahvsttzScnuICMIhvVhsci3sBY623RPUTLot-vw-4C-TnJEzn9BRlKv_1mlTap8pjZaJibzmnYfqIFJTtx0J8JbNTAAJZT40md8s2l9mFA-YkAwMK3c3J3D6836fXb4IxvxsLBZlW8bwn10ae9QKtvFOLqYKNUuDB70aBn70vunRecF9d3dRSJ45IPhnLCH43EMKPe7kjmoZUSCrGtRW7567Pa2IKtPcOUSXFK5rEaFxh8zdo4_oa_TQN5OdeXD-tU7TFQMxYSH8CtJlCEu6pEOOsXmYrY9KEHXsnx10EiLcZ4gp1k8KevV7f3Cwp1wrrhpSUySe5lslazbqcQjuhgyKEalktZEhNC93xinnAs9YxK9PPRTWaJ8CzYP-hwv8wc9WMDxQBU7Zd44F2lowlWQyjfOYil-DZxvfZxtxKIKHbnxZp5ATfFxjTRTONPfYakJrk7keliUkV0Nk1QdMBQhoQ_PvYpvH3z2_IGKfimL8nhvm_farJR_PcGkivf0fkkWC0s20RT2fYdIFLRqEGPG5IXLzgzXKfLpKqzIcdSGQT3diSqalIJHL_hK2a-a093dYeiFglgezKipyphRan_APpb4eAIqXzh7jrRZWiGvFEuXjFgj3r7o5zGyWXgzFqYs7GiPI4l.37rii6CDHnmVXB-ygST7SQ";
 "qbo_access_token_expires_in" = "2018-02-27 11:32:45";
 "qbo_client_id" = Q0oINt5LXcrqqbovx6sELLuA74bjxleUGp0OvtbielwNU14v5e;
 "qbo_client_secret" = Jq2ILTF2Awj81snQKUMGy7ZVQi3bHIpLsdg3r6Ba;
 "qbo_company_id" = 1;
 "qbo_connect_status" = 1;
 "qbo_refresh_token" = Q01152845390963w1EWI4iK73yOkBKVzkinvsOwDipRYJSGy7W;
 "qbo_refresh_token_expires_in" = "2018-06-08 11:31:50";
 "stripe_private_key" = "sk_test_TvHqFM4IDUZO2C3QCB5XgZgp";
 "stripe_public_key" = "pk_test_tuib6Y0as9sYnDWZXWddMZqR";
 "updated_at" = "2018-02-27 10:32:45";
 }, "plan": {
 "created_at" = "2017-09-22 05:58:45";
 currency = usd;
 description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
 id = 8;
 name = "Golden Plans";
 planid = goldenplans;
 price = "2000.00";
 status = 0;
 "updated_at" = "2018-02-14 11:19:34";
 validity = 2;
 "validity_interval" = year;
 }, "ip_address": 202.164.56.82, "card": 0, "updated_at": 2018-03-13 09:37:36, "token": vdOpSxN9Gz0U9vHo, "email": charles@mailinator.com, "address": {
 "address_one" = "Test Street222\n";
 "address_two" = "";
 city = city222;
 "created_at" = "2017-09-22 05:54:52";
 id = 99;
 link = "";
 "postal_code" = 923422;
 state = state222;
 "updated_at" = "2018-01-16 08:04:44";
 "user_id" = 116;
 }, "status": 1, "role_id": 1, "first_name": charlesadminone, "created_at": 2017-09-22 05:54:52, "company_slug": G5qqLYepo4PCDRq8Fo0juQ==, "subscriptionId": sub_BRfKySZNYxx6q2, "stripe_id": cus_BRfKNd7jnnJE1h, "qbo_customer_id": 64]

 
 
 */



