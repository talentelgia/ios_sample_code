//
//  Helper.swift
//  BaseSetup
//
//  Created by Talentelgia on 1/19/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import Foundation
import UIKit
class Helper{
    static func screenShotMethod()->UIImage{
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return screenshot!
    }
    
    static func showAlert(title:String, subtitle:String){
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil))
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    class func topMostController() -> UIViewController {
        var topController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController
        while ((topController?.presentedViewController) != nil) {
            topController = topController?.presentedViewController
        }
        return topController!
    }
    
    class func setPlaceHolderColor(textField:UITextField) {
        textField.attributedPlaceholder = NSAttributedString(string: "Email",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
    }
    
    // MARK:- Convert Date string into local format
    class func convertDateToLocal(inputDate:String,inputFormat:String,outputFormat:String)-> String{
        if inputDate.count  > 0 {
            let inputDateFormatter = DateFormatter()
            inputDateFormatter.dateFormat = inputFormat
            inputDateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
            let date = inputDateFormatter.date(from: inputDate)
            let outputDateFormatter = DateFormatter()
            outputDateFormatter.dateFormat = outputFormat
            outputDateFormatter.timeZone = NSTimeZone.system
            if date != nil {
                let outputDate = outputDateFormatter.string(from: date!)
                return outputDate
            }
        }
        return ""
    }
    
    
    
    // MARK:- Convert Date to desired format
    class func convertDateFormater_String(date:NSDate,inputdateformat:String,outputDateFormat:String,timeZone:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputdateformat
        dateFormatter.timeZone = NSTimeZone.system
        let date = date
        dateFormatter.dateFormat = outputDateFormat
        dateFormatter.timeZone = NSTimeZone.system
        let sortedDate = dateFormatter.string(from: date as Date)
        return sortedDate
    }
    
    
    // MARK:- Convert Date to desired format
    class func convertDateFormater(date:NSDate,outputDateFormat:String,timeZone:String) -> String {
        let dateFormatter = DateFormatter()
        let date = date
        dateFormatter.dateFormat = outputDateFormat
        dateFormatter.timeZone = NSTimeZone.system
        let sortedDate = dateFormatter.string(from: date as Date)
        return sortedDate
    }
    
    
    class func getExpiray() -> String {
        var expDays:Int = 0
        var retStr = ""
        let currentDate = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let endStr = Helper.convertDateFormater(date: currentDate, outputDateFormat: KOutputformat, timeZone: "")
        let currentStr =   Helper.convertDateToLocal(inputDate: ShareData.getUserData().planPurchaseDate, inputFormat:KInputformat , outputFormat:KOutputformat)
        print(currentStr)
        print(endStr)
        let start = dateFormatter.date(from: currentStr)
        let end = dateFormatter.date(from: endStr)
        let diff = Date.daysBetween(start: start!, end: end!) //
        print(diff)
        print(ShareData.getUserData().validityInterval)
        if ShareData.getUserData().validityInterval == "day" {
            expDays = Int(ShareData.getUserData().validity)! - diff
            
            if expDays > 365 {
                let days = Float(expDays)
                let year = days/365.0
                let roundValue = Int(year.rounded())
                print(roundValue)
            retStr = "Current plan expires in \(roundValue) Year "
            }else {
                retStr = "Current plan expires in \(expDays) days "
            }
            
        }else if ShareData.getUserData().validityInterval == "month" {
            
          let days =  Int(ShareData.getUserData().validity)! * 30 - diff
            expDays = days - diff
            if expDays > 365 {
                let days = Float(expDays)
                let year = days/365.0
                let roundValue = Int(year.rounded())
                print(roundValue)
                var myMutableString = NSMutableAttributedString()
                retStr = "Current plan expires in \(roundValue) Year "
            }else {
                retStr = "Current plan expires in \(expDays) days "
            }
        }else if ShareData.getUserData().validityInterval == "year" {
            let days = Int(ShareData.getUserData().validity)! * 365
            expDays = days - diff
            if expDays > 365 {
                let days = Float(expDays)
                let year = days/365.0
                let roundValue = Int(year.rounded())
                print(roundValue)
                retStr = "Current plan expires in \(roundValue) Year "
            }else {
                retStr = "Current plan expires in \(expDays) days "
            }
        }
        print(retStr)
        return retStr
    }
    
    class func alert(title:String, subtitle:String){
        let alert=UIAlertController(title: title, message: subtitle, preferredStyle: .alert);
        let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
            
        }
        alert.addAction(cancelAction)
        self.topMostController().present(alert, animated: true, completion: nil);
    }
    
    
    
    //Show Loader
    
    class func showLoaderView()
    {
        let window : UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let controllerView : UIView = UIView(frame:CGRect(x:0,y:0,width:UIScreen .main.bounds.width, height:UIScreen.main.bounds.height))
        controllerView.tag = 100
        controllerView.backgroundColor = UIColor .clear
        
        let activityBackgroundView: UIView = UIView(frame: CGRect(x:0,y: 0,width: 100,height: 100))
        activityBackgroundView.center = window.center
        activityBackgroundView.backgroundColor = UIColor.lightGray
        activityBackgroundView.alpha = 1.0
        activityBackgroundView.clipsToBounds = true
        activityBackgroundView.layer.cornerRadius = 10
        
        let actIndicatorView : UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        actIndicatorView.transform = CGAffineTransform(scaleX: 2, y: 2)
        actIndicatorView.color = hexStringToUIColor(hex: "#EF6C01")
        actIndicatorView.center = activityBackgroundView.center
        actIndicatorView .startAnimating()
        
        let loadingLabel = UILabel()
        loadingLabel.backgroundColor = UIColor.clear
        loadingLabel.text = "Loading..."
        loadingLabel.textColor = UIColor.black
        loadingLabel.font = UIFont(name: "systemFont-Semibold", size: 25.0)
        loadingLabel.textColor = hexStringToUIColor(hex: "#EF6C01")
        loadingLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let titleConstraints: [NSLayoutConstraint] = [
            NSLayoutConstraint(item: loadingLabel, attribute: .left, relatedBy: .equal, toItem: activityBackgroundView, attribute: .left, multiplier: 1, constant: 15),
            NSLayoutConstraint(item: loadingLabel, attribute: .right, relatedBy: .equal, toItem: activityBackgroundView, attribute: .right, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: loadingLabel, attribute: .top, relatedBy: .equal, toItem: actIndicatorView, attribute: .top, multiplier: 1, constant: 25),
            NSLayoutConstraint(item: loadingLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
        ]
        controllerView.addConstraints(titleConstraints)
        controllerView.addSubview(activityBackgroundView)
        controllerView .addSubview(actIndicatorView)
        controllerView.addSubview(loadingLabel)
        window.addSubview(controllerView)
    }
    
    
    class func setPlaceHolderColorForAllFields(textField:UITextField,placeholder:String) {
        textField.attributedPlaceholder = NSAttributedString(string: placeholder,attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray])
    }
    
    //Hide loader
    
    class func hideLoaderView()
        
    {
        let window : UIWindow  =  ((UIApplication .shared.delegate?.window)!)!
        for loader : UIView in window.subviews
        {
            if loader.tag == 100
            {
                loader .removeFromSuperview()
                print("loader removed")
            }
        }
        
    }
    class func resizeImage(image: UIImage) -> UIImage {
        var actualHeight: Float = Float(image.size.height)
        var actualWidth: Float = Float(image.size.width)
        let maxHeight: Float = 300.0
        let maxWidth: Float = 400.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.5
        //50 percent compression
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x:0.0, y:0.0, width:CGFloat(actualWidth), height:CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = UIImageJPEGRepresentation(img!,CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!)!
    }
}
extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    var png: Data? { return UIImagePNGRepresentation(self) }
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
}
