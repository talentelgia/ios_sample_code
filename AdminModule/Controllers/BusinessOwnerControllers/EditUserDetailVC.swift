//
//  EditUserDetailVC.swift
//  ROOFMATE
//
//  Created by OSX on 05/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class EditUserDetailVC: UIViewController {
    
    //MARK:-Properties
    
    var editUserDetailView:EditUserDetailView = EditUserDetailView()
    var editServiceManager:EditUserDetailServiceManager = EditUserDetailServiceManager()
    var userData:UserModel = UserModel()
    var pickerController = UIImagePickerController()
    var imageView = UIImage()
    var businessOwnerVC:BusinessOwnerVC = BusinessOwnerVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.editUserDetailView = self.view as! EditUserDetailView
        
        editUserDetailView.delegate = self
        editServiceManager.delegate = self
        updateUI()
        // Enabling IQKeyBoardManager
        IQKeyboardManager.sharedManager().enable = true
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print(userData.imageName)
        self.editUserDetailView.tableViewContent(userData: userData)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
// MARK: - Extension For Custom methods
extension EditUserDetailVC{
    func updateUI(){
        
        //Add leftbar button on navigation bar
        let addButton = UIBarButtonItem(image:UIImage(named:"menu-icon"), style:.plain, target:self, action:#selector(DashBoardVC.leftBarButtonAction(_:)))
        addButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = addButton
        
        //Add rightbar button on navigation bar
        let optionButton = UIBarButtonItem(image:UIImage(named:"nav-dots"), style:.plain, target:self, action:#selector(DashBoardVC.rightBarButtonAction(_:)))
        optionButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = optionButton
        
        //Add Image on navigation bar
        var selectionImage = UIImage(named:"logo-white")
        let tabSize = CGSize(width: 180, height: 25)
        UIGraphicsBeginImageContext(tabSize)
        selectionImage?.draw(in: CGRect(x: 0, y: 0, width: tabSize.width, height: tabSize.height))
        selectionImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let imageView = UIImageView(image:selectionImage)
        self.navigationItem.titleView = imageView
        
    }
}


//MARK:- UIButton Actions
extension EditUserDetailVC{
    
    @objc func leftBarButtonAction(_ sender: UIBarButtonItem) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        nextViewController.image = Helper.screenShotMethod()
        self.navigationController?.pushViewController(nextViewController, animated: false)
    }
    
    @objc func rightBarButtonAction(_ sender: UIBarButtonItem) {
        // Create the AlertController and add its actions like button in ActionSheet
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let profileActionButton = UIAlertAction(title: "Profile", style: .default) { action -> Void in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        actionSheetController.addAction(profileActionButton)
        let changePasswordActionButton = UIAlertAction(title: "Change Password", style: .default) { action -> Void in
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        actionSheetController.addAction(changePasswordActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Sign Out", style: .destructive) { action -> Void in
            let userEmail = ShareData.getUserData().email
            
            ShareData.saveUserData(urlData: UserModel())
            
            let userData:UserModel = UserModel()
            
            userData.email = userEmail
            
            ShareData.saveUserData(urlData: userData)
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let controller = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let navigationController = UINavigationController(rootViewController: controller)
            appDelegate.window!.rootViewController = navigationController
        }
        actionSheetController.addAction(deleteActionButton)
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
}

extension EditUserDetailVC:EditUserDetailViewDelegate,UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    
    func editUserDetailViewDelegate(_ view: EditUserDetailView, userData: UserModel, didPressUpdateButton sender: Any) {
        print(userData.imageName)
        editServiceManager.callEditProfileService(loginData: userData)
    }
    
    func editUserDetailViewDelegate(_ view: EditUserDetailView, didPressCancelButton sender: Any) {
        self.tabBarController?.selectedIndex = 1
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    func editUserDetailViewDelegate(_ view: EditUserDetailView) {
        let alertViewController = UIAlertController(title: "Choose your option", message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Camera", style: .default, handler: { (alert) in
            self.openCamera()
        })
        let gallery = UIAlertAction(title: "Gallery", style: .default) { (alert) in
            self.openGallary()
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (alert) in
            
        }
        alertViewController.addAction(camera)
        alertViewController.addAction(gallery)
        alertViewController.addAction(cancel)
        self.present(alertViewController, animated: true, completion: nil)
        
    }
    
    func openGallary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            pickerController.delegate = self
            pickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            pickerController.allowsEditing = true
            self.present(pickerController, animated: true, completion: nil)
        }
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            pickerController.delegate = self
            self.pickerController.sourceType = UIImagePickerControllerSourceType.camera
            pickerController.allowsEditing = true
            self .present(self.pickerController, animated: true, completion: nil)
        }
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        imageView = info[UIImagePickerControllerEditedImage] as! UIImage
        editUserDetailView.updateProfileImage(imgDp: imageView)
        dismiss(animated:true, completion: nil)
    }
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

extension EditUserDetailVC:EditUserDetailServiceManagerDelegate{
    func editUserDetailServiceManagerDelegate(_ sender: EditUserDetailServiceManager, status: Int) {
        let alert = UIAlertController(title: KAPP_NAME, message: KUpdatedDataAlert, preferredStyle: UIAlertControllerStyle.alert)
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) in
            DispatchQueue.main.async {
                
                self.businessOwnerVC.isReload = true
                self.tabBarController?.selectedIndex = 1
                self.navigationController?.popToRootViewController(animated: false)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
