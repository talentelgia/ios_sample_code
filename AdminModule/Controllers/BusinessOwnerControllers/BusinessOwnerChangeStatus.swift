//
//  BusinessOwnerChangeStatus.swift
//  ROOFMATE
//
//  Created by OSX on 08/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit
protocol BusinessOwnerChangeStatusDelegate:class{
    func businessOwnerChangeStatusDelegate(_ view:BusinessOwnerChangeStatus, didPressUpdateButton sender:Int)
}
class BusinessOwnerChangeStatus: UIViewController {
    
    //MARK:-Properties
    
    var businessChangeStatus:BusinessOwnerChangeView = BusinessOwnerChangeView()
    var delegate:BusinessOwnerChangeStatusDelegate?
    var imgUser:UIImage = UIImage()
    var arrSelectedUsers:[String] = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.businessChangeStatus = self.view as! BusinessOwnerChangeView
        businessChangeStatus.updateUI(imgIcon: imgUser)
        self.businessChangeStatus.delegate = self
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension BusinessOwnerChangeStatus:BusinessOwnerChangeViewDelegate{
    func businessOwnerChangeViewDelegate(_ view: BusinessOwnerChangeView, didPressRadiButton sender: Int) {
        if sender != 0{
            if checkUsers(){
                self.delegate?.businessOwnerChangeStatusDelegate(self, didPressUpdateButton: sender)
                DispatchQueue.main.async {
                    self.navigationController?.navigationBar.isHidden = false
                    self.tabBarController?.tabBar.isHidden = false
                    self.navigationController?.popViewController(animated: false)
                }
            }
        }
        else{
            Helper.alert(title: KAPP_NAME, subtitle: kSelectedAction)
        }
        
    }
    
    func businessOwnerChangeViewDelegate(_ view: BusinessOwnerChangeView, didPressCrossButton sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.navigationBar.isHidden = false
            self.tabBarController?.tabBar.isHidden = false
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func checkUsers() -> Bool {
        if arrSelectedUsers.count >  0{
            return true
        }
        else{
            
            let alert = UIAlertController(title: KAPP_NAME, message: kSelectCheckBox, preferredStyle: UIAlertControllerStyle.alert)
            // add the actions (buttons)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) in
                DispatchQueue.main.async {
                    self.tabBarController?.tabBar.isHidden = false
                    
                    self.navigationController?.navigationBar.isHidden = false
                    self.navigationController?.popViewController(animated: false)
                }
            }))
            self.present(alert, animated: true, completion: nil)
            return false
        }
    }
    
}
