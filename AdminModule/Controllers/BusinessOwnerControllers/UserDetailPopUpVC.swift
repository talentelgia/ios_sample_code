//
//  UserDetailPopUpVC.swift
//  ROOFMATE
//
//  Created by OSX on 05/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit

class UserDetailPopUpVC: UIViewController {
    
    //MARK:-Properties
    
    var userDetailView:UserDetailPopUpView = UserDetailPopUpView()
    var imgUser:UIImage = UIImage()
    var userData:UserModel = UserModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userDetailView = self.view as! UserDetailPopUpView
        self.userDetailView.delegate = self
        userDetailView.updateUI(imgIcon: imgUser)
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
        userDetailView.tableViewContent(userData: userData)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension UserDetailPopUpVC:UserDetailPopUpViewDelegate{
    func userDetailPopUpViewDelegate(_ view: UserDetailPopUpView, didPressCrossButton sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.navigationBar.isHidden = false
            self.tabBarController?.tabBar.isHidden = false
            self.navigationController?.popViewController(animated: false)
            
        }
    }
    
    
}
