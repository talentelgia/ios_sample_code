//
//  BusinessUserSortVC.swift
//  ROOFMATE
//
//  Created by OSX on 09/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit

protocol BusinessUserSortVCDelegate:class{
    func businessUserSortVCDelegate(_ UIViewController:BusinessUserSortVC, didPressBtn sender:Int)
}
class BusinessUserSortVC: UIViewController {
    
    //MARK:-Properties
    
    var image: UIImage?
    var businessUserSortView:BusinessUserSortView = BusinessUserSortView()
    var delegate:BusinessUserSortVCDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.businessUserSortView = self.view as! BusinessUserSortView
        self.businessUserSortView.updateUI(imgIcon:image!)
        self.businessUserSortView.delegate = self
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension BusinessUserSortVC:BusinessUserSortViewDelgate {
    
    func businessUserSortViewDelgate(_ view: BusinessUserSortView, didPressCloseBtn sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.navigationBar.isHidden = false
            self.tabBarController?.tabBar.isHidden = false
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func businessUserSortViewDelgate(_ view: BusinessUserSortView, didPressNameBtn sender: Any) {
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = false
        self.delegate?.businessUserSortVCDelegate(self, didPressBtn: 1)
        self.navigationController?.popViewController(animated: false)
    }
    
    func businessUserSortViewDelgate(_ view: BusinessUserSortView, didPressStatusBtn sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.navigationBar.isHidden = false
            self.tabBarController?.tabBar.isHidden = false
            self.delegate?.businessUserSortVCDelegate(self, didPressBtn: 2)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func businessUserSortViewDelgate(_ view: BusinessUserSortView, didPressEmailBtn sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.navigationBar.isHidden = false
            self.tabBarController?.tabBar.isHidden = false
            self.delegate?.businessUserSortVCDelegate(self, didPressBtn: 3)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func businessUserSortViewDelgate(_ view: BusinessUserSortView, didPressCompanyBtn sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.navigationBar.isHidden = false
            self.tabBarController?.tabBar.isHidden = false
            self.delegate?.businessUserSortVCDelegate(self, didPressBtn: 4)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func businessUserSortViewDelgate(_ view: BusinessUserSortView, didPressPlanNameBtn sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.navigationBar.isHidden = false
            self.tabBarController?.tabBar.isHidden = false
            self.delegate?.businessUserSortVCDelegate(self, didPressBtn: 5)
            self.navigationController?.popViewController(animated: false)
        }
    }
}

