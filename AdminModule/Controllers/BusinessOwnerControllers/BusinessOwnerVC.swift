//
//  BusinessOwnerVC.swift
//  ROOFMATE
//
//  Created by OSX on 02/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit

class BusinessOwnerVC: UIViewController {
    
    //MARK:-Properties
    
    var businessOwner:BusinessOwnerView = BusinessOwnerView()
    var businessOwnerServiceManager:BusinessOwnerServiceManager = BusinessOwnerServiceManager()
    var  businessOwnerChangeStatus:BusinessOwnerChangeStatus = BusinessOwnerChangeStatus()
    
    var refreshControl = UIRefreshControl()
    var startDate:Date?
    var endDate:Date?
    var newOffset:Int = Int()
    var isRecordExistStatus:Int = Int()
    var isReload:Bool = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.businessOwner = self.view as! BusinessOwnerView
        self.businessOwner.actIndView.isHidden = true
        businessOwnerServiceManager.delegate = self
        businessOwner.delegate = self
        businessOwnerChangeStatus.delegate = self
        updateUI()
        // Set up the refresh control
        self.refreshControl.addTarget(self, action: #selector(ManagePaymentVC.onRefreshBtnTapped(_:)), for:.valueChanged)
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            businessOwner.tblBusinessOwners.refreshControl = refreshControl
        } else {
            businessOwner.tblBusinessOwners.addSubview(refreshControl)
        }
        callServicemanager()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isReload == true {
            isReload = false
            callServicemanager()
        }
    }
    
    @objc func onRefreshBtnTapped(_ sender: Any) {
        self.businessOwner.dataDict.removeAll()
        self.businessOwner.sortArr.removeAllObjects()
        businessOwnerServiceManager.callRefreshService(refreshControl: self.refreshControl)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

// MARK: - Extension For Custom methods
extension BusinessOwnerVC{
    func updateUI(){
        
        //Add leftbar button on navigation bar
        let addButton = UIBarButtonItem(image:UIImage(named:"menu-icon"), style:.plain, target:self, action:#selector(DashBoardVC.leftBarButtonAction(_:)))
        addButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = addButton
        
        //Add rightbar button on navigation bar
        let optionButton = UIBarButtonItem(image:UIImage(named:"nav-dots"), style:.plain, target:self, action:#selector(DashBoardVC.rightBarButtonAction(_:)))
        optionButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = optionButton
        
        //Add Image on navigation bar
        var selectionImage = UIImage(named:"logo-white")
        let tabSize = CGSize(width: 180, height: 25)
        UIGraphicsBeginImageContext(tabSize)
        selectionImage?.draw(in: CGRect(x: 0, y: 0, width: tabSize.width, height: tabSize.height))
        selectionImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let imageView = UIImageView(image:selectionImage)
        self.navigationItem.titleView = imageView
        
    }
}

//MARK:- UIButton Actions
extension BusinessOwnerVC{
    
    @objc func leftBarButtonAction(_ sender: UIBarButtonItem) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        nextViewController.image = Helper.screenShotMethod()
        self.navigationController?.pushViewController(nextViewController, animated: false)
    }
    
    @objc func rightBarButtonAction(_ sender: UIBarButtonItem) {
        // Create the AlertController and add its actions like button in ActionSheet
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let profileActionButton = UIAlertAction(title: "Profile", style: .default) { action -> Void in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        actionSheetController.addAction(profileActionButton)
        let changePasswordActionButton = UIAlertAction(title: "Change Password", style: .default) { action -> Void in
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        actionSheetController.addAction(changePasswordActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Sign Out", style: .destructive) { action -> Void in
            let userEmail = ShareData.getUserData().email
            
            ShareData.saveUserData(urlData: UserModel())
            
            let userData:UserModel = UserModel()
            
            userData.email = userEmail
            
            ShareData.saveUserData(urlData: userData)
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let controller = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let navigationController = UINavigationController(rootViewController: controller)
            appDelegate.window!.rootViewController = navigationController
        }
        actionSheetController.addAction(deleteActionButton)
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
}

//MARK: - Custom Methods
extension BusinessOwnerVC{
    func callServicemanager(){
        businessOwnerServiceManager.callGetOwnerListService()
    }
}


extension BusinessOwnerVC:BusinessOwnerServiceManagerDelegate{
    func businessOwnerServiceManagerDelegate(_ sender: BusinessOwnerServiceManager, result: [UserModel]?, resultArr: NSArray, status: Int, offset: Int) {
        newOffset = offset
        isRecordExistStatus = status
        DispatchQueue.main.async {
            self.businessOwner.updateUIComponents(result: result!, resultArr: resultArr, offset: self.newOffset, status: status)
        }
    }
    
    func businessOwnerServiceManagerEmpltyResponseDelegateChangeStatus(_ sender: BusinessOwnerServiceManager) {
        self.businessOwner.updateUIComponenetsOnEmptyResponse()
    }
    
    func businessOwnerServiceManagerDelegateChangeStatus(_ sender: BusinessOwnerServiceManager, status: Int) {
        let alert = UIAlertController(title: title,
                                      message: kStatusUpdated,
                                      preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            self.businessOwner.dataDict.removeAll()
            self.businessOwner.sortArr = []
            self.callServicemanager()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func businessOwnerServiceManagerDelegate(_ sender: BusinessOwnerServiceManager, status: Int) {
        let alert = UIAlertController(title: title,
                                      message: kUserDeleted,
                                      preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            self.businessOwner.dataDict.removeAll()
            self.businessOwner.sortArr = []
            self.callServicemanager()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}

/*
 ViewUser = 0
 EditUser = 1
 EditCompany = 2
 DeleteUser = 3
 updatestatusSingleStatus = 4
 */

extension BusinessOwnerVC:BusinessOwnerViewDelegate{
    func businessOwnerViewDelegate(_ view: BusinessOwnerView, userData: UserModel, isDelete: Int, didPressLoginButton sender: Int) {
        let view: Int = sender
        switch view {
        case 0:
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "UserDetailPopUpVC") as! UserDetailPopUpVC
            nextViewController.imgUser = Helper.screenShotMethod()
            nextViewController.userData = userData
            self.navigationController?.pushViewController(nextViewController, animated: false)
        case 1:
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "EditUserDetailVC") as! EditUserDetailVC
            nextViewController.userData = userData
            self.navigationController?.pushViewController(nextViewController, animated: false)
        case 2:
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "EditCompanyVC") as! EditCompanyVC
            nextViewController.userData = userData
            self.navigationController?.pushViewController(nextViewController, animated: false)
        case 3:
            let actionSheetController = UIAlertController(title:"Confirmation", message: "Are you sure do you want to delete?", preferredStyle: .actionSheet)
            
            let changePasswordActionButton = UIAlertAction(title: "Delete", style: .destructive) { action -> Void in
                
                let strId = userData.slug
                self.businessOwner.updateTableViewOnSameOffset(index: isDelete)
                self.businessOwnerServiceManager.callDeleteSingleUser(strUserIds: strId)
            }
            actionSheetController.addAction(changePasswordActionButton)
            let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
                print("Cancel")
            }
            actionSheetController.addAction(cancelActionButton)
            self.present(actionSheetController, animated: true, completion: nil)
            
            
        case 4:
            let strId = userData.user_id
            // self.businessOwner.dataDict.removeAll()
            if userData.status == "1"{
                businessOwnerServiceManager.callUpdateStatusSingleUser(strUserId: strId, status: "1")
            }else{
                businessOwnerServiceManager.callUpdateStatusSingleUser(strUserId: strId, status: "0")
            }
        default:
            print("Some other character")
        }
    }
    
    func businessOwnerViewDelegate(_ view: BusinessOwnerView, didPressSortButton sender: Int) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BusinessUserSortVC") as! BusinessUserSortVC
        nextViewController.image = Helper.screenShotMethod()
        nextViewController.delegate = self
        self.navigationController?.pushViewController(nextViewController, animated: false)
    }
    
    func businessOwnerViewDelegate(_ view: BusinessOwnerView, setNewOffset sender:Int) {
        businessOwnerServiceManager.callGetOwnersListWithOffset(offset: sender)
    }
    
    
    func businessOwnerViewDelegate(_ view: BusinessOwnerView, didPressFilterButton sender: Int) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ManageBusinessOwnerDateFilterVC") as! ManageBusinessOwnerDateFilterVC
        nextViewController.image = Helper.screenShotMethod()
        nextViewController.delegate = self
        self.navigationController?.pushViewController(nextViewController, animated: false)
    }
    
    
    
    
    func businessOwnerViewDelegate(_ view: BusinessOwnerView, selectedValues: String, updateStatus sender: Int) {
        
        let view: Int = sender
        switch view {
        case 1:
            businessOwnerServiceManager.callChangeStatusUsers(strUserIds: selectedValues, status:"1")
        case 2:
            businessOwnerServiceManager.callChangeStatusUsers(strUserIds: selectedValues, status:"0" )
        case 3:
            self.businessOwnerServiceManager.callDeleteUsers(strUserIds: selectedValues)
            
        default:
            print("Some other character")
        }
        
    }
    
    func businessOwnerViewDelegate(_ view: BusinessOwnerView, didPressStatusButton sender: [String]) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BusinessOwnerChangeStatus") as! BusinessOwnerChangeStatus
        nextViewController.imgUser = Helper.screenShotMethod()
        nextViewController.delegate = self
        nextViewController.arrSelectedUsers = sender
        self.navigationController?.pushViewController(nextViewController, animated: false)
    }
    
    func businessOwnerViewDelegate(_ view: BusinessOwnerView, userData: UserModel, didPressLoginButton sender: Int) {
        
    }
}


extension BusinessOwnerVC:BusinessOwnerChangeStatusDelegate{
    func businessOwnerChangeStatusDelegate(_ view: BusinessOwnerChangeStatus, didPressUpdateButton sender: Int) {
        businessOwner.updateStatusOfUser(value: sender)
    }
}

extension BusinessOwnerVC:BusinessUserSortVCDelegate{
    func businessUserSortVCDelegate(_ UIViewController: BusinessUserSortVC, didPressBtn sender: Int) {
        
        let view: Int = sender
        switch view {
        case 1:
            self.sortingTransactions(sortingKey: "first_name")
        case 2:
            self.sortingTransactions(sortingKey: "status")
        case 3:
            self.sortingTransactions(sortingKey: "email")
        case 4:
            self.sortingTransactions(sortingKey: "company.name")
        case 5:
            self.sortingTransactions(sortingKey: "subscription_plan.name")
        default:
            print("Some other character")
        }
    }
    
    func sortingTransactions(sortingKey:String) {
        print(self.businessOwner.sortArr.count)
        let descriptor: NSSortDescriptor = NSSortDescriptor(key: sortingKey, ascending: true,selector: #selector(NSString.caseInsensitiveCompare(_:)))
        let descriptors: NSArray = [descriptor]
        let sortedArray = self.businessOwner.sortArr.sortedArray(using: descriptors as! [NSSortDescriptor])
        print(sortedArray.count)
        let result:[UserModel]? = Parser.ownersJsonToModel(dict: sortedArray as! [[String : Any]])
        print(result?.count)
        print(sortedArray.count)
        self.businessOwner.sortArr.removeAllObjects()
        self.businessOwner.updateUIComponentsOffset(result: result!, resultArr: sortedArray as NSArray, offset: self.newOffset, status: isRecordExistStatus)
    }
}

extension BusinessOwnerVC:ManageBusinessOwnerDateFilterVCDelegate{
    func manageBusinessOwnerDateFilterVCDelegate(_ UIViewController: ManageBusinessOwnerDateFilterVC, startDate: String, endDate: String, didPressSearchBtn sender: Any) {
        
        if (startDate.trim() == "" )
        {
            Helper.alert(title: KAPP_NAME, subtitle: kStartDateAlert)
        }
        else if (endDate.trim() == "")
        {
            Helper.alert(title: KAPP_NAME, subtitle: kEndDateDateAlert)
        }else{
            businessOwner.dataDict.removeAll()
            businessOwner.sortArr = []; businessOwnerServiceManager.callGetOwnerListByRangeService(start: startDate, end: endDate)
        }
    }
    
    func manageBusinessOwnerDateFilterVCDelegate(_ UIViewController: ManageBusinessOwnerDateFilterVC, didPressResetBtn sender: Any) {
        self.callServicemanager()
    }
    
}
