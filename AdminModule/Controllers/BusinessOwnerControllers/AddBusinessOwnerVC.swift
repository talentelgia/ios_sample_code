//
//  EditUserDetailVC.swift
//  ROOFMATE
//
//  Created by OSX on 05/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit

class AddBusinessOwnerVC: UIViewController {
    
    
    //MARK:-Properties
    
    
    var addBusinessOwnerView:AddBusinessOwnerView = AddBusinessOwnerView()
    var addBusinessUserServiceManager:AddBusinessUserServiceManager = AddBusinessUserServiceManager()
    var userData:UserModel = UserModel()
    var dataForStatus:[String] = ["Active","Inactive"]
    var strValue:String = String()
    var businessOwnerVC:BusinessOwnerVC = BusinessOwnerVC()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBusinessOwnerView = self.view as! AddBusinessOwnerView
        addBusinessOwnerView.delegate = self
        addBusinessUserServiceManager.delegate = self
        updateUI()
        self.addBusinessOwnerView.tableViewContent(userData: userData)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
// MARK: - Extension For Custom methods
extension AddBusinessOwnerVC{
    func updateUI(){
        
        //Add leftbar button on navigation bar
        let addButton = UIBarButtonItem(image:UIImage(named:"menu-icon"), style:.plain, target:self, action:#selector(DashBoardVC.leftBarButtonAction(_:)))
        addButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = addButton
        
        //Add rightbar button on navigation bar
        let optionButton = UIBarButtonItem(image:UIImage(named:"nav-dots"), style:.plain, target:self, action:#selector(DashBoardVC.rightBarButtonAction(_:)))
        optionButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = optionButton
        
        //Add Image on navigation bar
        var selectionImage = UIImage(named:"logo-white")
        let tabSize = CGSize(width: 180, height: 25)
        UIGraphicsBeginImageContext(tabSize)
        selectionImage?.draw(in: CGRect(x: 0, y: 0, width: tabSize.width, height: tabSize.height))
        selectionImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let imageView = UIImageView(image:selectionImage)
        self.navigationItem.titleView = imageView
        
    }
}


//MARK:- UIButton Actions
extension AddBusinessOwnerVC{
    
    @objc func leftBarButtonAction(_ sender: UIBarButtonItem) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        nextViewController.image = Helper.screenShotMethod()
        self.navigationController?.pushViewController(nextViewController, animated: false)
    }
    
    @objc func rightBarButtonAction(_ sender: UIBarButtonItem) {
        // Create the AlertController and add its actions like button in ActionSheet
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let profileActionButton = UIAlertAction(title: "Profile", style: .default) { action -> Void in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        actionSheetController.addAction(profileActionButton)
        let changePasswordActionButton = UIAlertAction(title: "Change Password", style: .default) { action -> Void in
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        actionSheetController.addAction(changePasswordActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Sign Out", style: .destructive) { action -> Void in
            let userEmail = ShareData.getUserData().email
            
            ShareData.saveUserData(urlData: UserModel())
            
            let userData:UserModel = UserModel()
            
            userData.email = userEmail
            
            ShareData.saveUserData(urlData: userData)
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let controller = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let navigationController = UINavigationController(rootViewController: controller)
            appDelegate.window!.rootViewController = navigationController
        }
        actionSheetController.addAction(deleteActionButton)
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
}

extension AddBusinessOwnerVC:AddBusinessOwnerViewDelegate,UIGestureRecognizerDelegate,UINavigationControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate{
    
    func addPlanViewDelegate(_ view: AddBusinessOwnerView, didPickerView sender: Int) {
        
        // data = dataForStatus
        let alertView = UIAlertController(
            title: "Select item from list",
            message: "\n\n\n\n\n\n\n\n\n",
            preferredStyle: .actionSheet)
        
        let pickerView = UIPickerView(frame:
            CGRect(x: 0, y: 50, width: alertView.view.frame.size.width, height: 162))
        pickerView.dataSource = self
        pickerView.delegate = self
        
        // comment this line to use white color
        pickerView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        
        alertView.view.addSubview(pickerView)
        
        let action = UIAlertAction(title: "OK", style: .default) { action -> Void in
            self.addBusinessOwnerView.resetData(strValue: self.strValue, index: sender)
        }
        
        alertView.addAction(action)
        self.present(alertView, animated: true) {
            //   pickerView.frame.size.width = alertView.view.frame.size.width
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataForStatus.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        self.strValue = dataForStatus[row]
        return dataForStatus[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
    func AddBusinessOwnerViewDelegate(_ view: AddBusinessOwnerView) {
    }
    
    
    func AddBusinessOwnerViewDelegate(_ view: AddBusinessOwnerView, userData: UserModel, didPressUpdateButton sender: Any) {
        addBusinessUserServiceManager.callSaveService(loginData: userData)
    }
    func AddBusinessOwnerViewDelegate(_ view: AddBusinessOwnerView, didPressCancelButton sender: Any) {
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.selectedIndex = 1
        self.navigationController?.popToRootViewController(animated: false)
    }
    
}

extension AddBusinessOwnerVC:AddBusinessUserServiceManagerDelegate{
    
    func AddBusinessUserServiceManagerDelegate(_ sender: AddBusinessUserServiceManager, status: Int) {
        let alert = UIAlertController(title: KAPP_NAME, message: KAddBusinessUser_Alert, preferredStyle: UIAlertControllerStyle.alert)
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) in
            DispatchQueue.main.async {
                self.businessOwnerVC.isReload = true
                self.tabBarController?.tabBar.isHidden = false
                self.tabBarController?.selectedIndex = 1
                self.navigationController?.popToRootViewController(animated: false)
            }
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func AddBusinessUserServiceManagerDelegate(_ sender: AddBusinessUserServiceManager, result: [UserModel]?, resultArr: NSArray, status: Int) {
        
        /*
         let alert = UIAlertController(title: KAPP_NAME, message: KUpdatedDataAlert, preferredStyle: UIAlertControllerStyle.alert)
         // add the actions (buttons)
         alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) in
         DispatchQueue.main.async {
         self.tabBarController?.selectedIndex = 3
         self.navigationController?.popToRootViewController(animated: false)
         }
         }))
         self.present(alert, animated: true, completion: nil)
         
         */
    }
    
    func AddBusinessUserServiceManagerDelegateEmptyResponseDelegate(_ sender: AddBusinessUserServiceManager) {
        print("Empty")
    }
    
    
}




