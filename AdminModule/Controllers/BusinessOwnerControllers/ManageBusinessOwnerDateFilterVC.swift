//
//  ManageBusinessOwnerDateFilterVC.swift
//  ROOFMATE
//
//  Created by OSX on 12/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

protocol ManageBusinessOwnerDateFilterVCDelegate:class{
    
    //MARK:-Properties
    
    func manageBusinessOwnerDateFilterVCDelegate(_ UIViewController:ManageBusinessOwnerDateFilterVC,startDate:String,endDate:String, didPressSearchBtn sender:Any)
    func manageBusinessOwnerDateFilterVCDelegate(_ UIViewController:ManageBusinessOwnerDateFilterVC, didPressResetBtn sender:Any)
}
class ManageBusinessOwnerDateFilterVC: UIViewController {
    
    var image: UIImage?
    var manageBusinessOwnerDateFilterView:ManageBusinessOwnerDateFilterView = ManageBusinessOwnerDateFilterView()
    let inView = UIView()
    var delegate:ManageBusinessOwnerDateFilterVCDelegate?
    var startDate:String = ""
    var endDate:String = ""
    var startDay:Date?
    var endDay:Date?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Enabling IQKeyBoardManager
        IQKeyboardManager.sharedManager().enable = true
        self.manageBusinessOwnerDateFilterView = self.view as! ManageBusinessOwnerDateFilterView
        self.manageBusinessOwnerDateFilterView.updateUI(imgIcon:image!)
        self.manageBusinessOwnerDateFilterView.setUPDatepicker()
        
        self.manageBusinessOwnerDateFilterView.delegate = self
        
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension ManageBusinessOwnerDateFilterVC:ManageBusinessOwnerDateFilterViewDelgate {
    func manageBusinessOwnerDateFilterViewDelgate(_ view: ManageBusinessOwnerDateFilterView, isStartTxtFld: Bool) {
        if isStartTxtFld {
            print("start")
            self.manageBusinessOwnerDateFilterView.datePicker.addTarget(self, action: #selector(ManageBusinessOwnerDateFilterVC.datePickerValueChanged), for: UIControlEvents.valueChanged)
        }else {
            print("End")
            self.manageBusinessOwnerDateFilterView.datePicker.addTarget(self, action: #selector(ManageBusinessOwnerDateFilterVC.datePickerValueChanged), for: UIControlEvents.valueChanged)
        }
    }
    
    func manageBusinessOwnerDateFilterViewDelgate(_ view: ManageBusinessOwnerDateFilterView, didPressCloseBtn sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.navigationBar.isHidden = false
            self.tabBarController?.tabBar.isHidden = false
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func manageBusinessOwnerDateFilterViewDelgate(_ view: ManageBusinessOwnerDateFilterView, didPressResetBtn sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.navigationBar.isHidden = false
            self.tabBarController?.tabBar.isHidden = false
            self.delegate?.manageBusinessOwnerDateFilterVCDelegate(self, didPressResetBtn: sender)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func manageBusinessOwnerDateFilterViewDelgate(_ view: ManageBusinessOwnerDateFilterView, didPressSearchBtn sender: Any) {
        if (startDate.trim() == "" )
        {
            Helper.alert(title: KAPP_NAME, subtitle: kStartDateAlert)
        }
        else if (endDate.trim() == "")
        {
            Helper.alert(title: KAPP_NAME, subtitle: kEndDateDateAlert)
        }
        else if self.endDay! < self.startDay!
        {
            Helper.alert(title: KAPP_NAME, subtitle: KMaxDateAlert)
        }
        else{
            DispatchQueue.main.async {
                self.navigationController?.navigationBar.isHidden = false
                self.tabBarController?.tabBar.isHidden = false
                self.delegate?.manageBusinessOwnerDateFilterVCDelegate(self, startDate: self.startDate, endDate: self.endDate, didPressSearchBtn: sender)
                self.navigationController?.popViewController(animated: false)
            }
        }
    }
    
    
    
    //ToolBar Cancel Button action method.
    @objc func onDoneBtnTapped(){
        print("Done button pressed")
        self.inView.removeFromSuperview()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    //MARK:- UIDatePicker Value Changed.
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        
        if self.manageBusinessOwnerDateFilterView.isStart{
            self.startDay = sender.date as Date
            self.manageBusinessOwnerDateFilterView.startTxtFld.text! = Helper.convertDateFormater_String(date: sender.date as NSDate, inputdateformat:"MMM dd,yyyy",outputDateFormat:"dd-MM-yyyy",timeZone:"")
            
            self.startDate = Helper.convertDateFormater_String(date: sender.date as NSDate, inputdateformat:"MMM dd,yyyy",outputDateFormat:"yyyy-MM-dd",timeZone:"")
            
        }else {
            self.endDay = sender.date as Date
            self.manageBusinessOwnerDateFilterView.endtxtFld.text! = Helper.convertDateFormater_String(date: sender.date as NSDate, inputdateformat:"MMM dd,yyyy",outputDateFormat:"dd-MM-yyyy",timeZone:"")
            
            self.endDate = Helper.convertDateFormater_String(date: sender.date as NSDate, inputdateformat:"MMM dd,yyyy",outputDateFormat:"yyyy-MM-dd",timeZone:"")
        }
    }
    
}

