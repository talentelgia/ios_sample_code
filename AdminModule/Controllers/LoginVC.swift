//
//  LoginVC.swift
//  ROOFMATE
//  Created by Talentelgia on 1/30/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    
    //MARK:-Properties
    var loginView:LoginView = LoginView()
    var loginserviceManager:LoginServiceManager = LoginServiceManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loginView = self.view as! LoginView
        self.loginView.SetupView()
        self.loginView.delegate = self
        self.loginserviceManager.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        //self.title = "Login"
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension LoginVC:LoginViewDelgate {
    
    
    func loginViewDelagate(_ view: LoginView, didPressForgotBtn sender: Any) {
        self.view.endEditing(true)
        print("Move to Forgot Password controller")
    }
    
    
    func loginViewDelagate(_ view: LoginView, loginData: LoginModel, didPressLoginButton sender: Any) {
        
        loginserviceManager.callSignInService(loginData: loginData)
    }
    
}

extension LoginVC:LoginServiceManagerDelgate{
    
    func loginServiceManagerDelgate(_ sender: LoginServiceManager, result: UserModel?, status: Int) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc: SuperAdminTabBarController = mainStoryboard.instantiateViewController(withIdentifier: "SuperAdminTabBarController") as! SuperAdminTabBarController
        vc.selectedIndex = 0
        self.present(vc, animated: false, completion: nil)
    }
    
}




