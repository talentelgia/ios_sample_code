//
//  LoginServiceManager.swift
//  Crown Gospel
//
//  Created by mac min  on 28/12/17.
//  Copyright © 2017 mac min . All rights reserved.
//

import Foundation

struct LoginModel {
    var userName:String = ""
    var password:String = ""
}

protocol LoginServiceManagerDelgate:class {
    
    func loginServiceManagerDelgate(_ sender:LoginServiceManager, result:UserModel?,status:Int)
}

class LoginServiceManager{
    
    weak var delegate:LoginServiceManagerDelgate?
    
    
    private func getParams(_ loginData:LoginModel)->NSDictionary{
        let dict:[String:Any] = ["email":loginData.userName,"password":loginData.password]
        
        print(dict)
       
        return dict as NSDictionary
        
    }
    private func isValidResponse(_ data:[String : Any]?)->Bool{
        if data != nil{
            if let statusCode:Int = data?["status"] as? Int{
                if statusCode == 200{
                    return true
                }else if statusCode == 400 {
                    if let msg:String = data?["message"] as? String{
                        Helper.showAlert(title: KAPP_NAME, subtitle: msg)
                    }
                    
                    return false
                }

            }
        Helper.showAlert(title: "Error", subtitle: "Some issue occured. Please try after some time.")
        return false
        }
         return false
    }
    
    func callSignInService(loginData:LoginModel) {
        ConnectionManager.shareInstance.CallWebServiceWithOutHeader(methodType: "POST", methodName: KLoginUrl, inputDict: getParams(loginData), completion: { (response) in
            
            if self.isValidResponse(response){
                
                print(response)
                let resultData:[String : Any] = response["data"] as! [String : Any]
                
                print(resultData)
                //parsing here
            ShareData.saveUserData(urlData:Parser.userJsonToModel(dict: resultData))
                print("yes")
                
                self.delegate?.loginServiceManagerDelgate(self, result: Parser.userJsonToModel(dict: resultData),status: 1)
            }
        }) { (message) in
            print(message)
        }
        
    }
}
