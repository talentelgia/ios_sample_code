//
//  EditUserDetailServiceManager.swift
//  ROOFMATE
//
//  Created by OSX on 07/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import Foundation

protocol EditUserDetailServiceManagerDelegate:class {
    
    func editUserDetailServiceManagerDelegate(_ sender:EditUserDetailServiceManager,status:Int)
}

class EditUserDetailServiceManager{
    
    weak var delegate:EditUserDetailServiceManagerDelegate?
    
    
    private func getParams(_ profileData:UserModel)->NSDictionary{
        print(profileData.imageName)
        
         let address:[String:AnyObject] = ["address_one":profileData.address.address_one as AnyObject,"address_two":profileData.address.address_two as AnyObject,"state":profileData.address.state as AnyObject,"postal_code":profileData.address.postal_code as AnyObject,"city":profileData.address.city as AnyObject,"id":profileData.address.id as AnyObject,"link":profileData.address.link as AnyObject]
        
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: address, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            
            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
            // here "decoded" is of type `Any`, decoded from JSON data
            
            // you can now cast it with the right type
            if let address = decoded as? [String:String] {
                print(address)
                // use dictFromJSON
            }
        } catch {
            print(error.localizedDescription)
        }
        
        
        
        let dict:[String:AnyObject] = ["first_name":profileData.first_name as AnyObject,"last_name":profileData.last_name as AnyObject,"email":profileData.email as AnyObject, "id":profileData.user_id as AnyObject as AnyObject,"role_id":profileData.user_type as AnyObject,"phone_number":profileData.phone_number as AnyObject,"address":address as AnyObject,"profile_image":profileData.imgBase64 as AnyObject,"is_base64":true as AnyObject,"password":profileData.password as AnyObject]
        
        print(dict)
        return dict as NSDictionary
        
    }
    
    
    
    private func isValidResponse(_ data:[String : Any]?)->Bool{
        if data != nil{
            if let statusCode:Int = data?["status"] as? Int{
                if statusCode == 200{
                    return true
                }else if statusCode == 400{
                    if let msg:String = data?["message"] as? String{
                        Helper.showAlert(title: KAPP_NAME, subtitle: msg)
                    }
                    
                    return false
                }
                else if statusCode == -1{
                    Helper.showAlert(title: "Error", subtitle: "Some issue occured. Please try after some time.")
                    
                    return false
                }
                else if statusCode == -2{
                    Helper.showAlert(title: "Error", subtitle: "Email and  password didn't matched.")
                    
                    return false
                }
            }
        }
        Helper.showAlert(title: "Error", subtitle: "Some issue occured. Please try after some time.")
        return false
    }
    
    func callEditProfileService(loginData:UserModel) {
        
        ConnectionManager.shareInstance.CallWebService(methodType: "POST", methodName: KUpdateProfile, inputDict: getParams(loginData), completion: { (response) in
            print(response)
            if self.isValidResponse(response){
                self.delegate?.editUserDetailServiceManagerDelegate(self, status: 1)
            }
        }) { (message) in
            print("Message")
        }
    }

    
}
