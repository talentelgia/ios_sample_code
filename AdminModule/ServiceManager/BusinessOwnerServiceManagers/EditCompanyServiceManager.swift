//
//  EditCompanyServiceManager.swift
//  ROOFMATE
//
//  Created by OSX on 08/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import Foundation
protocol EditCompanyServiceManagerDelegate:class {
    func editCompanyServiceManagerDelegate(_ sender:EditCompanyServiceManager,status:Int)
}

class EditCompanyServiceManager{
    
    weak var delegate:EditCompanyServiceManagerDelegate?
    
    
    private func getParams(_ profileData:UserModel)->NSDictionary{
      
        let dict:[String:AnyObject] = ["id":profileData.company.compId as AnyObject,"name":profileData.company.comName as AnyObject,"stripe_private_key":profileData.company.stripe_private_key as AnyObject, "stripe_public_key":profileData.company.stripe_public_key as AnyObject as AnyObject,"qbo_client_id":profileData.company.qbo_client_id as AnyObject,"qbo_client_secret":profileData.company.qbo_client_secret as AnyObject,"qbo_realmID":profileData.company.qbo_company_id as AnyObject,"logo":profileData.imgBase64 as AnyObject,"is_base64":true as AnyObject]
        print(dict)
        return dict as NSDictionary
        
    }
    
    
    
    private func isValidResponse(_ data:[String : Any]?)->Bool{
        if data != nil{
            if let statusCode:Int = data?["status"] as? Int{
                if statusCode == 200{
                    return true
                }else if statusCode == 400{
                    if let msg:String = data?["message"] as? String{
                        Helper.showAlert(title: KAPP_NAME, subtitle: msg)
                    }
                    
                    return false
                }
                else if statusCode == -1{
                    Helper.showAlert(title: "Error", subtitle: "Some issue occured. Please try after some time.")
                    
                    return false
                }
                else if statusCode == -2{
                    Helper.showAlert(title: "Error", subtitle: "Email and  password didn't matched.")
                    
                    return false
                }
            }
        }
        Helper.showAlert(title: "Error", subtitle: "Some issue occured. Please try after some time.")
        return false
    }
    
    func callEditCompanyService(userData:UserModel) {

        ConnectionManager.shareInstance.CallWebService(methodType: "POST", methodName: KUpdateCompany, inputDict: getParams(userData), completion: { (response) in
            print(response)
            if self.isValidResponse(response){
               self.delegate?.editCompanyServiceManagerDelegate(self, status: 1)
            }
        }) { (message) in
            print("Message")
        }
    }
    
}
