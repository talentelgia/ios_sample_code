//
//  ManageOfficeServiceManager.swift
//  ROOFMATE
//
//  Created by OSX on 12/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import Foundation
import UIKit


protocol AddBusinessUserServiceManagerDelegate:class {
    
    func AddBusinessUserServiceManagerDelegate(_ sender:AddBusinessUserServiceManager, result:[UserModel]?,resultArr: NSArray,status:Int)
    
    func AddBusinessUserServiceManagerDelegate(_ sender:AddBusinessUserServiceManager,status:Int)
    
    func AddBusinessUserServiceManagerDelegateEmptyResponseDelegate(_ sender:AddBusinessUserServiceManager)
    
}

class AddBusinessUserServiceManager{
    
    
    weak var delegate:AddBusinessUserServiceManagerDelegate?
    
    private func getParams(_ profileData:UserModel)->NSDictionary{
        
        let address:[String:AnyObject] = ["address_one":profileData.address.address_one as AnyObject,"state":profileData.address.state as AnyObject,"city":profileData.address.city as AnyObject]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: address, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            
            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
            // here "decoded" is of type `Any`, decoded from JSON data
        } catch {
            print(error.localizedDescription)
        }
        var strStatus:String = String()
        if profileData.status == "Active"{
            strStatus = "1"
        }else{
            strStatus = "0"
        }
        let dict:[String:AnyObject] = ["first_name":profileData.first_name as AnyObject,"last_name":profileData.last_name as AnyObject,"email":profileData.email as AnyObject, "address":address as AnyObject,"status":strStatus as AnyObject,"password":profileData.password as AnyObject,"name":profileData.company.comName as AnyObject]
        return dict as NSDictionary
    }
    
    
    private func isValidResponse(_ data:[String : Any]?)->Bool{
        if data != nil{
            if let statusCode:Int = data?["status"] as? Int{
                if statusCode == 200{
                    return true
                }else if statusCode == 400{
                    if let msg:String = data?["message"] as? String{
                        Helper.showAlert(title: KAPP_NAME, subtitle: msg)
                    }
                    
                    return false
                }
            }
        }
        Helper.showAlert(title: "Error", subtitle: "Some issue occured. Please try after some time.")
        return false
    }
    
    func callSaveService(loginData:UserModel) {
        
        ConnectionManager.shareInstance.CallWebService(methodType: "POST", methodName: KAddBusinessUser, inputDict: getParams(loginData), completion: { (response) in
            if self.isValidResponse(response){
                self.delegate?.AddBusinessUserServiceManagerDelegate(self, status: 1)
            }
        }) { (message) in
            print("Message")
        }
    }
    
    
}


