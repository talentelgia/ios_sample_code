//
//  BusinessOwnerServiceManager.swift
//  ROOFMATE
//
//  Created by OSX on 02/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import Foundation
import UIKit

protocol BusinessOwnerServiceManagerDelegate:class {
    
    func businessOwnerServiceManagerDelegate(_ sender:BusinessOwnerServiceManager, result:[UserModel]?,resultArr: NSArray,status:Int,offset:Int)
    func businessOwnerServiceManagerDelegate(_ sender:BusinessOwnerServiceManager,status:Int)
    func businessOwnerServiceManagerDelegateChangeStatus(_ sender:BusinessOwnerServiceManager,status:Int)
    func businessOwnerServiceManagerEmpltyResponseDelegateChangeStatus(_ sender:BusinessOwnerServiceManager)
    
}

class BusinessOwnerServiceManager{
    
    weak var delegate:BusinessOwnerServiceManagerDelegate?
    
    private func isValidResponse(_ data:[String : Any]?)->Bool{
        if data != nil{
            if let statusCode:Int = data?["status"] as? Int{
                if statusCode == 200{
                    return true
                }else if statusCode == 400{
                    if let msg:String = data?["message"] as? String{
                        Helper.showAlert(title: KAPP_NAME, subtitle: msg)
                    }
                    
                    return false
                }
                else if statusCode == -1{
                    Helper.showAlert(title: "Error", subtitle: "Some issue occured. Please try after some time.")
                    
                    return false
                }
                else if statusCode == -2{
                    Helper.showAlert(title: "Error", subtitle: "Email and  password didn't matched.")
                    
                    return false
                }
            }
        }
        Helper.showAlert(title: "Error", subtitle: "Some issue occured. Please try after some time.")
        return false
    }
    
    func callGetOwnerListService() {
    
        ConnectionManager.shareInstance.CallGetWebService(methodType: "GET", methodName: KBusinessOwnerList, completion: { (response) in
            print(response["next_offset"] as? Int)
            if self.isValidResponse(response){
                
                let resultData:[[String : Any]] = response["data"] as! [[String : Any]]
                print(resultData)
                  let responseArr = response["data"] as? NSArray ?? []
                if responseArr.count == 0{
                    self.delegate?.businessOwnerServiceManagerEmpltyResponseDelegateChangeStatus(self)
                }else{
              
                    self.delegate?.businessOwnerServiceManagerDelegate(self, result: Parser.ownersJsonToModel(dict: resultData), resultArr: responseArr, status: (response["is_records_exists"] as? Int)!, offset: (response["next_offset"] as? Int)!)
                }
                
            }
        }) { (message) in
        }
    }
    
    func callGetOwnerListByRangeService(start:String,end:String) {
        
        let dict:[String:Any] = [KStartdateKey:start,KEnddateKey:end]
        print(dict)
        
        ConnectionManager.shareInstance.CallWebService(methodType: "POST", methodName: KGetBusinessOwnersByDate, inputDict: dict as NSDictionary, completion: { (response) in
            print(response)
            if self.isValidResponse(response){
                
                
                let resultData:[[String : Any]] = response["data"] as! [[String : Any]]
                print(resultData)
                let responseArr = response["data"] as? NSArray ?? []
                if responseArr.count == 0{
                    self.delegate?.businessOwnerServiceManagerEmpltyResponseDelegateChangeStatus(self)
                }else{
                    print(response["is_records_exists"])
            self.delegate?.businessOwnerServiceManagerDelegate(self, result: Parser.ownersJsonToModel(dict: resultData), resultArr: responseArr, status: (response["is_records_exists"] as? Int)!, offset: (response["next_offset"] as? Int)!)
                    
                    
                }
                
            }
        }) { (message) in
            print("Message")
        }
    }
    
    func callDeleteUsers(strUserIds:String) {
        
        let str = String(format:"%@%@/users",KDeleteMultipleUsers,strUserIds)
        print(str)
        ConnectionManager.shareInstance.CallGetWebServiceWithOutLoader(methodType: "Delete", methodName: str, completion: { (response) in
            
            if self.isValidResponse(response){
                 self.delegate?.businessOwnerServiceManagerDelegate(self, status: 1)
            }
        }) { (message) in
        }
    }
    
    func callChangeStatusUsers(strUserIds:String,status:String) {
        
        let str = String(format:"%@%@/users?status=%@",KChangeStatusUsers,strUserIds,status)
        print(str)
        ConnectionManager.shareInstance.CallGetWebService(methodType: "Put", methodName: str, completion: { (response) in
            print(response)
            if self.isValidResponse(response){
               self.delegate?.businessOwnerServiceManagerDelegateChangeStatus(self, status: 1)
            }
        }) { (message) in
        }
    }
    
    func callDeleteSingleUser(strUserIds:String) {
        
        let str = String(format:"%@%@",KDeleteSingleUser,strUserIds)
        print(str)
        ConnectionManager.shareInstance.CallGetWebServiceWithOutLoader(methodType: "Delete", methodName: str, completion: { (response) in
            
            if self.isValidResponse(response){
               
            //  self.delegate?.businessOwnerServiceManagerDelegate(self, status: 1)
            }
        }) { (message) in
        }
    }
    
    func callUpdateStatusSingleUser(strUserId:String,status:String) {
        let dict:[String:AnyObject] = ["status":status as AnyObject]
           let str = String(format:"%@%@",KUpdateSingleUserStatus,strUserId)
        print(str)
        ConnectionManager.shareInstance.CallWebServicePostWithoutLoader(methodType: "POST", methodName: str, inputDict: dict as NSDictionary, completion: { (response) in
            print(response)
            if self.isValidResponse(response){
                //self.callGetOwnerListService()
            }
        }) { (message) in
            print("Message")
        }
    }
    
    func callRefreshService(refreshControl:UIRefreshControl!) {
        
        ConnectionManager.shareInstance.CallRefreshGetWebService(methodType: "GET", methodName: KBusinessOwnerList, completion: { (response) in
            if self.isValidResponse(response){
                
                print(response)
                
                DispatchQueue.main.async(execute: {
                    if refreshControl.isRefreshing
                    {
                        refreshControl.endRefreshing()
                    }
                })
                
                
                let resultData:[[String : Any]] = response["data"] as! [[String : Any]]
                print(resultData)
                let responseArr = response["data"] as? NSArray ?? []
                if responseArr.count == 0{
                    self.delegate?.businessOwnerServiceManagerEmpltyResponseDelegateChangeStatus(self)
                }
                else{
                         self.delegate?.businessOwnerServiceManagerDelegate(self, result: Parser.ownersJsonToModel(dict: resultData), resultArr: responseArr, status: (response["is_records_exists"] as? Int)!, offset: (response["next_offset"] as? Int)!)
                }
            }
            
        }) { (message) in
            print("Message")
        }
        
    }
   // businessowner/mobile?offset=" + offset
    func callGetOwnersListWithOffset(offset:Int) {
          let str = String(format:"%@?offset=%d",KBusinessOwnerList,offset)
        print(str)
        ConnectionManager.shareInstance.CallRefreshGetWebService(methodType: "GET", methodName: str, completion: { (response) in
            if self.isValidResponse(response){
                let resultData:[[String : Any]] = response["data"] as! [[String : Any]]
                print(resultData)
                let responseArr = response["data"] as? NSArray ?? []
                if responseArr.count == 0{
                    self.delegate?.businessOwnerServiceManagerEmpltyResponseDelegateChangeStatus(self)
                }
                else{
                        self.delegate?.businessOwnerServiceManagerDelegate(self, result: Parser.ownersJsonToModel(dict: resultData), resultArr: responseArr, status: (response["is_records_exists"] as? Int)!, offset: (response["next_offset"] as? Int)!)
                }
            }
            
        }) { (message) in
            print("Message")
        }
        
    }
    
    
}
