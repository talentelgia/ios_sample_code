//
//  LoginView.swift
//  ROOFMATE
//
//  Created by Talentelgia on 1/30/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit

protocol LoginViewDelgate:class {
    
    func loginViewDelagate(_ view:LoginView, loginData:LoginModel,didPressLoginButton sender:Any)
    
   func loginViewDelagate(_ view:LoginView,didPressForgotBtn sender:Any)
    
}

class LoginView: UIView {
    
    //MARK:-IBOutlets

    @IBOutlet var emailTxtFld: UITextField!
    @IBOutlet var pwdTxtFld: UITextField!
    @IBOutlet var rememberBtn: UIButton!
    @IBOutlet var signInBtn: UIButton!
    
    weak var delegate:LoginViewDelgate?
    
    func SetupView()  {
        self.signInBtn.layer.cornerRadius = 2
        self.signInBtn.clipsToBounds = true
  
    }
    func isEmplty() -> Bool {
        
        if(emailTxtFld.text?.trim().isEmpty ?? false && pwdTxtFld.text?.trim().isEmpty ?? false)
        {
            return false
        }
        return true
    }
    
    func setLoginData() -> LoginModel {
        return LoginModel.init(userName: emailTxtFld.text!, password: pwdTxtFld.text!)
    }
    
    //MARK:-UIButton Action Methods.
    
    @IBAction func onSignInBtnTapped(_ sender: Any) {
        
        self.endEditing(true)
        
        if (!emailTxtFld.hasText)
        {
            Helper.showAlert(title: KAPP_NAME, subtitle:kEmail_Alert)
        }
        else if(!(emailTxtFld.text?.isEmail)!){
            Helper.showAlert(title: KAPP_NAME, subtitle:kValidemail_Alert)
        }
        else if(!pwdTxtFld.hasText){
            Helper.showAlert(title: KAPP_NAME, subtitle: kPassword_Alert)
        }
        else {
            
            delegate?.loginViewDelagate(self, loginData:self.setLoginData(), didPressLoginButton: sender)
        }
    }
    
    @IBAction func onForgotPwdBtnTapped(_ sender: Any) {
        
        delegate?.loginViewDelagate(self, didPressForgotBtn: sender)
        
    }
    
    
}
