//
//  EditUserDetailView.swift
//  ROOFMATE
//
//  Created by OSX on 05/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit
protocol EditUserDetailViewDelegate:class {
    func editUserDetailViewDelegate(_ view:EditUserDetailView,userData:UserModel, didPressUpdateButton sender:Any)
    func editUserDetailViewDelegate(_ view:EditUserDetailView, didPressCancelButton sender:Any)
    func editUserDetailViewDelegate(_ view:EditUserDetailView)
}
class EditUserDetailView: UIView {
    
    //MARK:-IBOutlets
    
    @IBOutlet weak var tblUserDetail: UITableView!
    
    var dataDict : [[String:Any]] = [[String:Any]]()
    weak var delegate:EditUserDetailViewDelegate?
    var imageName:String = String()
    var userData1:UserModel = UserModel()
    var profileImage:UIImage?
    var imageBase64:String = String()
    
}
extension EditUserDetailView{
    func tableViewContent(userData:UserModel){
         userData1 = userData
        dataDict = [["value":userData1.first_name,"title":"First name"],
                    ["value":userData1.last_name,"title":"Last  name"],
                    ["value":userData1.email,"title":"Email Id"],
                    ["value":userData1.phone_number,"title":"Phone Number"],
                    ["value":userData1.address.link,"title":"Website Link"],
                    ["value":userData1.address.address_one,"title":"Address 1"],
                    ["value":userData1.address.address_two,"title":"Address 2"],
                    ["value":userData1.address.city,"title":"City"],
                    ["value":userData1.address.state,"title":"State"],
                    ["value":userData1.address.postal_code,"title":"Postal Code"],  ["value":userData1.password,"title":"Password"]]

        imageName = userData1.imageName
       
        tblUserDetail.reloadData()
    }
    func updateProfileImage(imgDp:UIImage) {
      profileImage = imgDp
         tblUserDetail.reloadData()
    }
    
    func imageToBase64(img:UIImage) {
        let image : UIImage =  Helper.resizeImage(image:img)
        var strBase64:String = String()
        if let imageData = image.jpeg(.lowest) {
            strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        }
        imageBase64 = strBase64
        userData1.imgBase64 = imageBase64
    }
}
extension EditUserDetailView:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataDict.count  + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell:EditUserProfileImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! EditUserProfileImageTableViewCell
            
            if profileImage != nil{
                cell.imgProfile.image = profileImage
            }
            else{
                let str =  String(format:"%@%@",KImageBaseUrl,imageName as CVarArg)
                print(str)
                let url = URL(string: str)
                let image = UIImage(named: "no_Image")
                cell.imgProfile.kf.setImage(with: url, placeholder: image)
               
            }
            
            imageToBase64(img: cell.imgProfile.image!)

             cell.delegate = self
            return cell
        }else if (indexPath.row == dataDict.count + 1){
            let cell:EditProfilePopUpButtonsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! EditProfilePopUpButtonsTableViewCell
                cell.delegate = self
           // cell.updateValues(data: dataDict[indexPath.row  - 1])
            return cell
        }
        else{
            let cell:EditUserDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! EditUserDetailTableViewCell
               cell.txtValue.tag = indexPath.row
            if indexPath.row == 3{
                cell.txtValue.isEnabled = false
                cell.txtValue.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            }
            else{
                 cell.txtValue.isEnabled = true
                  cell.txtValue.backgroundColor = .clear
            }
              if indexPath.row == 4 || indexPath.row == 10 {
                cell.txtValue.keyboardType = .numberPad
            }
              else{
                cell.txtValue.keyboardType = .default
            }
            if indexPath.row == 11  {
                cell.txtValue.isSecureTextEntry = true
            }
            else{
                cell.txtValue.isSecureTextEntry = false
            }
            cell.txtValue.delegate = self
            
            cell.updateValues(data: dataDict[indexPath.row  - 1])
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == dataDict.count + 1){
            return 60
        }else if (indexPath.row == 0)
        {
              return  91
        }else{
              return  84
        }
    }
}
extension EditUserDetailView:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true);
        return false;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //do further customization with indexPath
      
        switch textField.tag {
        case 1:
            userData1.first_name = textField.text!.trim()
        case 2:
            userData1.last_name = textField.text!.trim()
//        case 3:
//            userData1.company.comName = textField.text!.trim()
//        case 4:
//            userData1.plan.planName = textField.text!.trim()
        case 3:
           userData1.email = textField.text!.trim()
        case 4:
           userData1.phone_number = textField.text!.trim()
        case 5:
            userData1.address.link = textField.text!.trim()
        case 6:
            userData1.address.address_one = textField.text!.trim()
        case 7:
            userData1.address.address_two = textField.text!.trim()
        case 8:
            userData1.address.city = textField.text!.trim()
        case 9:
            userData1.address.state = textField.text!.trim()
        case 10:
           userData1.address.postal_code = textField.text!.trim()
        case 11:
            userData1.password = textField.text!.trim()
        default: break
        }
        dataDict = [["value":userData1.first_name,"title":"First name"],
                    ["value":userData1.last_name,"title":"Last  name"],
                  
            ["value":userData1.email,"title":"Email Id"],
            ["value":userData1.phone_number,"title":"Phone Number"],
            ["value":userData1.address.link,"title":"Website Link"],
            ["value":userData1.address.address_one,"title":"Address 1"],
            ["value":userData1.address.address_two,"title":"Address 2"],
            ["value":userData1.address.city,"title":"City"],
            ["value":userData1.address.state,"title":"State"],
            ["value":userData1.address.postal_code,"title":"Postal Code"],  ["value":userData1.password,"title":"Password"]]
    }
}

extension EditUserDetailView:EditProfilePopUpButtonsTableViewCellDelegate{
    func editProfilePopUpButtonsTableViewCellDelegate(_ view: EditProfilePopUpButtonsTableViewCell, didPressUpdateButton sender: Any) {
       self.endEditing(true)
        if(userData1.first_name.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: kFirstName_Alert)
        }
        else if(userData1.last_name.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: klastName_Alert)
        }
        
        else if(userData1.phone_number.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: kPhoneNumber_Alert)
        }

        else if(userData1.address.address_one.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: kAddress_Alert)
        }
        else if(userData1.address.city.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: kCity_Alert)
        }
        else if(userData1.address.state.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: kState_Alert)
        }
        else if(userData1.address.postal_code.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: kPostalCode_Alert)
        }
        else if(userData1.password.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: KPasswordEmpty)
        }
        else{
             self.delegate?.editUserDetailViewDelegate(self, userData: userData1, didPressUpdateButton: sender)
        }
       
    }
    
    func editProfilePopUpButtonsTableViewCellDelegate(_ view: EditProfilePopUpButtonsTableViewCell, didPressCancelButton sender: Any){
            self.delegate?.editUserDetailViewDelegate(self, didPressCancelButton: sender)
    }
}
extension EditUserDetailView:EditUserProfileImageTableViewCellDelegate{
    func editUserProfileImageTableViewCellDelegate(_ view: EditUserProfileImageTableViewCell) {
        self.delegate?.editUserDetailViewDelegate(self)
    }
}
