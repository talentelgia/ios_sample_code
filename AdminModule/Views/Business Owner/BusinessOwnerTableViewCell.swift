//
//  BusinessOwnerTableViewCell.swift
//  ROOFMATE
//
//  Created by OSX on 02/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit
//MARK:- Custom Protocols
protocol BusinessOwnerTableViewCellDelegate:class {
    
    func businessOwnerTableViewCellDelagate( view:BusinessOwnerTableViewCell,didPressViewUserButon sender:Any)
    func businessOwnerTableViewCellDelagate( view:BusinessOwnerTableViewCell,didPressEditUserButon sender:Any)
    
    func businessOwnerTableViewCellDelagate( view:BusinessOwnerTableViewCell, didPressEditCompany sender:Any)
    
    func businessOwnerTableViewCellDelagate( view:BusinessOwnerTableViewCell, didPressDeleteUserButton sender:Any)
   
    
    func businessOwnerTableViewCellDelagate( view:BusinessOwnerTableViewCell, didPressSelectButton sender:Any)
    
       func businessOwnerTableViewCellDelagate( view:BusinessOwnerTableViewCell)
}

//MARK:- Cell Life cycle
class BusinessOwnerTableViewCell: UITableViewCell {
    
    //MARK:-IBOutlets
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblBusinessOwner: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblPlanName: UILabel!
    @IBOutlet weak var btnSelectOutLet: UIButton!
    
    weak var delegate:BusinessOwnerTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewBg.shadowlayer()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        lblStatus.isUserInteractionEnabled = true
        lblStatus.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
    func updateValues(data:UserModel,arrSelected:[String]){
        lblEmail.text = data.email
        lblBusinessOwner.text = String(format:"%@ %@",data.first_name,data.last_name)
        lblCompanyName.text = data.company.comName
        lblPlanName.text = data.plan.planName
        print(data.status)
        if data.status == "1"
        {
            lblStatus.text = "Active"
        }
        else{
            lblStatus.text = "Inactive"
        }
        
        let myString = data.user_id
        
       // let myInt = Int(myString)
        
        if arrSelected.contains(myString) {
            self.btnSelectOutLet.setImage(#imageLiteral(resourceName: "tick-green"), for: .normal)
        }else{
            self.btnSelectOutLet.setImage(#imageLiteral(resourceName: "tick-gray"), for: .normal)
        }
    }
 
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if lblStatus.text == "Active"{
            lblStatus.text = "Inactive"
        }
        else{
             lblStatus.text = "Active"
        }
        self.delegate?.businessOwnerTableViewCellDelagate(view: self)
    }
}

//MARK:- UIButtons Actions extensions
extension BusinessOwnerTableViewCell{
    @IBAction func BtnViewUser(_ sender: Any) {
       delegate?.businessOwnerTableViewCellDelagate(view: self, didPressViewUserButon: sender)
    }
    @IBAction func btnEditUser(_ sender: Any) {
        delegate?.businessOwnerTableViewCellDelagate(view: self, didPressEditUserButon: sender)
    }
    
    @IBAction func btnEditCompany(_ sender: Any) {
        delegate?.businessOwnerTableViewCellDelagate(view: self, didPressEditCompany: sender)
    }
    
    @IBAction func btnDeleteUser(_ sender: Any) {
        delegate?.businessOwnerTableViewCellDelagate(view:self, didPressDeleteUserButton: sender)
    }
    
    @IBAction func BtnSelect(_ sender: Any) {
        delegate?.businessOwnerTableViewCellDelagate(view: self, didPressSelectButton: sender)
    }
    
}
