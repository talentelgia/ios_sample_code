//
//  EditUserDetailView.swift
//  ROOFMATE
//
//  Created by OSX on 05/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit

protocol AddBusinessOwnerViewDelegate:class {
    
    func AddBusinessOwnerViewDelegate(_ view:AddBusinessOwnerView,userData:UserModel, didPressUpdateButton sender:Any)
    func AddBusinessOwnerViewDelegate(_ view:AddBusinessOwnerView, didPressCancelButton sender:Any)
    func AddBusinessOwnerViewDelegate(_ view:AddBusinessOwnerView)
     func addPlanViewDelegate(_ view:AddBusinessOwnerView, didPickerView sender:Int)
    
}

class AddBusinessOwnerView:UIView {
    
    //MARK:-IBOutlets
    
    @IBOutlet weak var tblUserDetail: UITableView!
    var dataDict : [[String:Any]] = [[String:Any]]()
    weak var delegate:AddBusinessOwnerViewDelegate?
    var userData1:UserModel = UserModel()
    
}
extension AddBusinessOwnerView{
    
    func tableViewContent(userData:UserModel){
        userData1 = userData
        dataDict = [["value":"","title":"First Name"],
                    ["value":"","title":"Last Name"],
                    ["value":"","title":"Email"],
                    ["value":"","title":"Company"],
                    ["value":"","title":"Address"],
                    ["value":"","title":"City"],
                    ["value":"","title":"State"],
                    ["value":"","title":"Status"],
                    ["value":"","title":"Password"]]
        tblUserDetail.reloadData()
    }
    
    func resetData(strValue:String,index:Int){
        
        userData1.status = strValue
       
        dataDict = [["value":userData1.first_name,"title":"First Name"],
                    ["value":userData1.last_name,"title":"Last Name"],
                    ["value":userData1.email,"title":"Email"],
                ["value":userData1.company.comName,"title":"Company"],
            ["value":userData1.address.address_one,"title":"Address"],
                    ["value":userData1.address.city,"title":"City"],
                    ["value":userData1.address.state,"title":"State"],
                    ["value":userData1.status,"title":"Status"],
                    ["value":userData1.password,"title":"Password"]]
        
        tblUserDetail.reloadData()
    }
    
}
extension AddBusinessOwnerView:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataDict.count  + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.row == dataDict.count  ){
            let cell:AddBusinessUserButtonsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! AddBusinessUserButtonsTableViewCell
            cell.delegate = self
            return cell
        }
        else{
            let cell:AddBusinessUserDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! AddBusinessUserDetailTableViewCell
            cell.txtValue.tag = indexPath.row
            if indexPath.row == 8{
                cell.txtValue.isSecureTextEntry = true
            }
            else{
                cell.txtValue.isSecureTextEntry = false
            }
            if indexPath.row == 7  {
                cell.downArrowImg.isHidden = false
                cell.txtValue.isEnabled = false
            } else  {
                cell.downArrowImg.isHidden = true
                cell.txtValue.isEnabled = true
            }
            
            
            cell.txtValue.delegate = self
            cell.updateValues(data: dataDict[indexPath.row ])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == dataDict.count){
            return 60
        }else{
            return  84
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == dataDict.count - 2{
            self.delegate?.addPlanViewDelegate(self, didPickerView: indexPath.row)
        }
    }
    
}

extension AddBusinessOwnerView:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true);
        return false;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        //do further customization with indexPath
        
        switch textField.tag {
        case 0:
            userData1.first_name = textField.text!.trim()
        case 1:
            userData1.last_name = textField.text!.trim()
        case 2:
            userData1.email = textField.text!.trim()
        case 3:
            userData1.company.comName = textField.text!.trim()
        case 4:
            userData1.address.address_one = textField.text!.trim()
        case 5:
            userData1.address.city = textField.text!.trim()
        case 6:
            userData1.address.state = textField.text!.trim()
        case 7:
            userData1.status = textField.text!.trim()
        case 8:
            userData1.password = textField.text!.trim()
            
        default: break
        }
        
        dataDict = [["value":userData1.first_name,"title":"First Name"],
                    ["value":userData1.last_name,"title":"Last Name"],
                    ["value":userData1.email,"title":"Email"],
                ["value":userData1.company.comName,"title":"Company"],
            ["value":userData1.address.address_one,"title":"Address"],
                    ["value":userData1.address.city,"title":"City"],
                    ["value":userData1.address.state,"title":"State"],
                    ["value":userData1.status,"title":"Status"],
                    ["value":userData1.password,"title":"Password"]]
    }
}


extension AddBusinessOwnerView:AddBusinessUserButtonsTableViewCellDelegate{
    
    func AddBusinessUserButtonsTableViewCellDelegate(_ view: AddBusinessUserButtonsTableViewCell, didPressUpdateButton sender: Any) {
        self.endEditing(true)
        if(userData1.first_name.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: kFirstName_Alert)
        }
        else if(userData1.last_name.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: klastName_Alert)
        }
        else if(userData1.email.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: kEmail_Alert )
        }
        else if(!(userData1.email.isEmail)){
            Helper.alert(title: KAPP_NAME, subtitle: kValidemail_Alert)
        }
        else if(userData1.company.comName.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle:KCompanyNameAlert )
        }
        else if(userData1.address.address_one.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: kAddress_Alert)
        }
        else if(userData1.address.city.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: kCity_Alert)
        }
        else if(userData1.address.state.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: kState_Alert)
        }
        else if(userData1.status.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: KStatus_Alert)
        }
        else if(userData1.password.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: kPassword_Alert)
        }
        else{
        self.delegate?.AddBusinessOwnerViewDelegate(self, userData: userData1, didPressUpdateButton: sender)
        }
    }
    
    func AddBusinessUserButtonsTableViewCellDelegate(_ view: AddBusinessUserButtonsTableViewCell, didPressCancelButton sender: Any) {
        print("cancel")
        self.delegate?.AddBusinessOwnerViewDelegate(self, didPressCancelButton: sender)
    }
}




