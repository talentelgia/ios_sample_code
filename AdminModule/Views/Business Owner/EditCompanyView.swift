//
//  EditCompanyView.swift
//  ROOFMATE
//
//  Created by OSX on 08/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit
protocol EditCompanyViewDelegate:class {
    func editCompanyViewDelegate(_ view:EditCompanyView,userData:UserModel, didPressUpdateButton sender:Any)
    func editCompanyViewDelegate(_ view:EditCompanyView, didPressCancelButton sender:Any)
    func editCompanyViewDelegate(_ view:EditCompanyView)
}
class EditCompanyView: UIView {
    
    //MARK:-IBOutlets
    
    @IBOutlet weak var tblUserDetail: UITableView!
    
    var dataDict : [[String:Any]] = [[String:Any]]()
    weak var delegate:EditCompanyViewDelegate?
    
    var imageName:String = String()
    var userData1:UserModel = UserModel()
    var profileImage:UIImage?
    var imageBase64:String = String()
    
}
extension EditCompanyView{
    func tableViewContent(userData:UserModel){
        userData1 = userData
        dataDict = [["value":userData1.company.comName,"title":"Company Name"],
                    ["value":userData1.company.stripe_private_key,"title":"Stripe Private key"],
                    ["value":userData1.company.stripe_public_key,"title":"Stripe Public key"],
                    ["value":userData1.company.qbo_client_id,"title":"QB Client Id"],
                    ["value":userData1.company.qbo_client_secret,"title":"QB Secret Id"],
                    ["value":userData1.company.qbo_company_id,"title":"QB Company Id"]]
        
        imageName = userData.company.logo
        print(imageName)
        tblUserDetail.reloadData()
    }
    func updateProfileImage(imgDp:UIImage) {
        profileImage = imgDp
        tblUserDetail.reloadData()
    }
    
    func imageToBase64(img:UIImage) {
        let image : UIImage =  Helper.resizeImage(image:img)
        var strBase64:String = String()
        if let imageData = image.jpeg(.lowest) {
            strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        }
        imageBase64 = strBase64
        userData1.imgBase64 = imageBase64
    }
}
extension EditCompanyView:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataDict.count  + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell:EditUserProfileImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! EditUserProfileImageTableViewCell
            
            if profileImage != nil{
                cell.imgProfile.image = profileImage
            }
            else{
                let str =  String(format:"%@%@",KCompanyImageUrl,imageName as CVarArg)
                print(str)
                let url = URL(string: str)
                let image = UIImage(named: "no_Image")
                cell.imgProfile.kf.setImage(with: url, placeholder: image)
                
            }
            
            imageToBase64(img: cell.imgProfile.image!)
            
            cell.delegate = self
            return cell
        }else if (indexPath.row == dataDict.count + 1){
            let cell:EditProfilePopUpButtonsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! EditProfilePopUpButtonsTableViewCell
            cell.delegate = self
            return cell
        }
        else{
            let cell:EditUserDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! EditUserDetailTableViewCell
            cell.txtValue.tag = indexPath.row
            cell.txtValue.delegate = self
            cell.updateValues(data: dataDict[indexPath.row  - 1])
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == dataDict.count + 1){
            return 60
        }else if (indexPath.row == 0)
        {
            return  91
        }else{
            return  84
        }
    }
}
extension EditCompanyView:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true);
        return false;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //do further customization with indexPath
        
        switch textField.tag {
        case 1:
            userData1.company.comName = textField.text!.trim()
        case 2:
            userData1.company.stripe_private_key = textField.text!.trim()
        case 3:
            userData1.company.stripe_public_key = textField.text!.trim()
        case 4:
            userData1.company.qbo_client_id = textField.text!.trim()
        case 5:
            userData1.company.qbo_client_secret = textField.text!.trim()
        case 6:
            userData1.company.qbo_company_id = textField.text!.trim()
        default: break
        }
        
        dataDict = [["value":userData1.company.comName,"title":"Company Name"],
                    ["value":userData1.company.stripe_private_key,"title":"Stripe Private key"],
                    ["value":userData1.company.stripe_public_key,"title":"Stripe Public key"],
                    ["value":userData1.company.qbo_client_id,"title":"QB Client Id"],
                    ["value":userData1.company.qbo_client_secret,"title":"QB Secret Id"],
                    ["value":userData1.company.qbo_company_id,"title":"QB Company Id"]]
    }
}

extension EditCompanyView:EditProfilePopUpButtonsTableViewCellDelegate{
    func editProfilePopUpButtonsTableViewCellDelegate(_ view: EditProfilePopUpButtonsTableViewCell, didPressUpdateButton sender: Any) {
        self.endEditing(true)
        if( userData1.company.comName.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: kCompanyName_Alert)
        }
        else if(userData1.company.stripe_private_key.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: kStripePrivateKey_Alert)
        }
        else if (userData1.company.stripe_public_key.trim() == "")
        {
            Helper.alert(title: KAPP_NAME, subtitle: kStripePublicKey_Alert)
        }
        else if(userData1.company.qbo_client_id.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: kQBCLientId_Alert)
        }
        else if(userData1.company.qbo_client_secret.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: kQBCLientSecret_Alert)
        }
        else if(userData1.company.qbo_company_id.trim() == ""){
            Helper.alert(title: KAPP_NAME, subtitle: kQBCLientCompanyID_Alert)
        }
        else{
             self.delegate?.editCompanyViewDelegate(self, userData: userData1, didPressUpdateButton: sender)
        }
    }
    
    func editProfilePopUpButtonsTableViewCellDelegate(_ view: EditProfilePopUpButtonsTableViewCell, didPressCancelButton sender: Any){
       self.delegate?.editCompanyViewDelegate(self, didPressCancelButton: sender)
    }
}
extension EditCompanyView:EditUserProfileImageTableViewCellDelegate{
    func editUserProfileImageTableViewCellDelegate(_ view: EditUserProfileImageTableViewCell) {
        self.delegate?.editCompanyViewDelegate(self)
    }
}

