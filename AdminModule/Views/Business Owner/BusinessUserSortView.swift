//
//  BusinessUserSortView.swift
//  ROOFMATE
//
//  Created by OSX on 09/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit
protocol BusinessUserSortViewDelgate:class {
    func businessUserSortViewDelgate(_ view:BusinessUserSortView,didPressCloseBtn sender:Any)
    func businessUserSortViewDelgate(_ view:BusinessUserSortView,didPressNameBtn sender:Any)
    func businessUserSortViewDelgate(_ view:BusinessUserSortView,didPressStatusBtn sender:Any)
    func businessUserSortViewDelgate(_ view:BusinessUserSortView,didPressEmailBtn sender:Any)
    func businessUserSortViewDelgate(_ view:BusinessUserSortView,didPressCompanyBtn sender:Any)
    func businessUserSortViewDelgate(_ view:BusinessUserSortView,didPressPlanNameBtn sender:Any)
    
}
class BusinessUserSortView: UIView {
    weak var delegate:BusinessUserSortViewDelgate?
    
    //MARK:-IBOutlets
    
    @IBOutlet var bgndImgView: UIImageView!
    
    
    //MARK:-UIButton Action Methods.
    
    @IBAction func onSortByNameTapped(_ sender: Any) {
     delegate?.businessUserSortViewDelgate(self, didPressNameBtn: sender)
    }
    
    
    @IBAction func onSortByStatusTapped(_ sender: Any) {
       delegate?.businessUserSortViewDelgate(self, didPressStatusBtn: sender)
    }
    
    @IBAction func onSortByEmailTapped(_ sender: Any) {
     delegate?.businessUserSortViewDelgate(self, didPressEmailBtn: sender)
    }
    
    @IBAction func onSortByCompanyTapped(_ sender: Any) {
        delegate?.businessUserSortViewDelgate(self, didPressCompanyBtn: sender)
    }
    
    @IBAction func onSortByPlanNameTapped(_ sender: Any) {
       delegate?.businessUserSortViewDelgate(self, didPressPlanNameBtn: sender)
    }
    
    @IBAction func onCloseBtnTapped(_ sender: Any) {
        
     delegate?.businessUserSortViewDelgate(self, didPressCloseBtn: sender)
    }
    
}

extension BusinessUserSortView{
    func updateUI(imgIcon:UIImage){
        bgndImgView.image = imgIcon
    }
    
}

