//
//  BusinessOwnerView.swift
//  ROOFMATE
//
//  Created by OSX on 02/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit
//MARK:- Custom Protocols
protocol BusinessOwnerViewDelegate:class {
    
    
    
    
    func businessOwnerViewDelegate(_ view:BusinessOwnerView, userData:UserModel,isDelete:Int,didPressLoginButton sender:Int)
    func businessOwnerViewDelegate(_ view:BusinessOwnerView, didPressStatusButton sender:[String])
    func businessOwnerViewDelegate(_ view:BusinessOwnerView,selectedValues:String, updateStatus sender:Int)
    func businessOwnerViewDelegate(_ view:BusinessOwnerView, didPressSortButton sender:Int)
    func businessOwnerViewDelegate(_ view:BusinessOwnerView, didPressFilterButton sender:Int)
    func businessOwnerViewDelegate(_ view:BusinessOwnerView, setNewOffset sender:Int)
}

//MARK:- View life cycle
class BusinessOwnerView: UIView {
    
    //MARK:-IBOutlets
    
    @IBOutlet weak var btnFilterOutlet: UIButton!
    @IBOutlet weak var tblBusinessOwners: UITableView!
    @IBOutlet weak var btnSortOutlet: UIButton!
    @IBOutlet weak var btnSelectAllOutlet: UIButton!
    @IBOutlet weak var btnSatusOutlet: UIButton!
    @IBOutlet var noRecordsLabel: UILabel!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet var actIndView: UIActivityIndicatorView!
    
    
    var dataDict:[UserModel] = [UserModel]()
    var delegate:BusinessOwnerViewDelegate?
    
    var arrSelectedRows:[String] = [String]()
    var sortArr = NSMutableArray()
    var newOffset:Int = Int()
    var isNewDataLoading = false
    var isRecordExist:Int = Int ()
   
}

extension BusinessOwnerView:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:BusinessOwnerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! BusinessOwnerTableViewCell
        cell.delegate = self
        if dataDict.count > 0 {
            cell.updateValues(data: dataDict[indexPath.row], arrSelected: arrSelectedRows)
        }
        return cell
    }
}

//MARK: - UIButtonActions
extension BusinessOwnerView{
    
    @IBAction func btnFilter(_ sender: Any) {
        
        self.delegate?.businessOwnerViewDelegate(self, didPressFilterButton: 1)
    }
    
    @IBAction func btnSort(_ sender: Any) {
        self.delegate?.businessOwnerViewDelegate(self, didPressSortButton: 1)
    }
    
    @IBAction func btnSelectAll(_ sender: Any) {
        
        if self.btnSelectAllOutlet.currentImage == #imageLiteral(resourceName: "tick-gray") {
            arrSelectedRows.removeAll()
            self.btnSelectAllOutlet.setImage(#imageLiteral(resourceName: "tick-green"), for: .normal)
            for obj in self.dataDict{
                let myString = obj.user_id
                // let myInt = Int(myString)
                arrSelectedRows.append(myString)
            }
        }else {
            self.btnSelectAllOutlet.setImage(#imageLiteral(resourceName: "tick-gray"), for: .normal)
            arrSelectedRows.removeAll()
        }
        self.tblBusinessOwners.reloadData()
        
    }
    
    @IBAction func btnStatus(_ sender: Any) {
        delegate?.businessOwnerViewDelegate(self, didPressStatusButton:arrSelectedRows)
    }
}

//MARK: - Custom Methods
extension BusinessOwnerView{
    func updateUIComponents(result: [UserModel],resultArr:NSArray,offset:Int,status:Int){
        DispatchQueue.main.async {
             self.actIndView.isHidden = true
            self.tblBusinessOwners.rowHeight = UITableViewAutomaticDimension
            self.tblBusinessOwners.estimatedRowHeight = 280
            self.noRecordsLabel.isHidden = true
            self.tblBusinessOwners.isHidden = false
            // self.sortArr = []
            self.btnSelectAllOutlet.setImage(#imageLiteral(resourceName: "tick-gray"), for: .normal)
            self.arrSelectedRows.removeAll()
            for obj in result{
                self.dataDict.append(obj)
            }
            
            self.isNewDataLoading = false
            for obj in resultArr{
                self.sortArr.add(obj)
            }
            print(self.sortArr.count)
            self.newOffset = offset
            self.isRecordExist = status
            self.tblBusinessOwners.reloadData()
        }
    }
    
    func updateUIComponentsOffset(result: [UserModel],resultArr:NSArray,offset:Int,status:Int){
        DispatchQueue.main.async {
               self.actIndView.isHidden = true
            self.tblBusinessOwners.rowHeight = UITableViewAutomaticDimension
            self.tblBusinessOwners.estimatedRowHeight = 280
            self.dataDict.removeAll()
            self.noRecordsLabel.isHidden = true
            self.tblBusinessOwners.isHidden = false
            self.sortArr = []
            self.btnSelectAllOutlet.setImage(#imageLiteral(resourceName: "tick-gray"), for: .normal)
            self.arrSelectedRows.removeAll()
            for obj in result{
                self.dataDict.append(obj)
            }
            
            self.isNewDataLoading = false
            for obj in resultArr{
                self.sortArr.add(obj)
            }
            print(self.sortArr.count)
            self.newOffset = offset
            self.isRecordExist = status
            self.tblBusinessOwners.reloadData()
        }
    }
    
    func updateUIComponenetsOnEmptyResponse()  {
        DispatchQueue.main.async {
               self.actIndView.isHidden = true
            self.tblBusinessOwners.rowHeight = UITableViewAutomaticDimension
            self.tblBusinessOwners.estimatedRowHeight = 280
            self.dataDict.removeAll()
            self.sortArr = []
            self.arrSelectedRows.removeAll()
            self.noRecordsLabel.isHidden = false
            self.noRecordsLabel.text = "No records found."
            //  self.tblBusinessOwners.isHidden = true
            self.tblBusinessOwners.reloadData()
        }
    }
    
    func updateStatusOfUser(value:Int){
        let joined = self.arrSelectedRows.joined(separator: ",")
        delegate?.businessOwnerViewDelegate(self, selectedValues: joined, updateStatus: value)
    }
    
    func updateTableViewOnSameOffset(index:Int){
        dataDict.remove(at: index)
        var total:CGFloat = CGFloat()
        total = 280 * CGFloat(dataDict.count)
        let offset = self.tblBusinessOwners.contentOffset.y + total
        self.tblBusinessOwners.reloadData()
        self.tblBusinessOwners.contentOffset.y = offset
        //tblBusinessOwners.reloadData()
    }
}

//MARK: - BusinessOwnerTableViewCellDelegate
extension BusinessOwnerView:BusinessOwnerTableViewCellDelegate{
    
    func businessOwnerTableViewCellDelagate(view: BusinessOwnerTableViewCell) {
        let indexPath = self.tblBusinessOwners.indexPathForRow(at: view.center)!
        if dataDict[indexPath.row].status == "1"{
             dataDict[indexPath.row].status = "0"
        }else{
              dataDict[indexPath.row].status = "1"
        }
         let userData1 = dataDict[indexPath.row]
        delegate?.businessOwnerViewDelegate(self, userData:userData1,isDelete:0, didPressLoginButton:4)
    }
    
    func businessOwnerTableViewCellDelagate(view: BusinessOwnerTableViewCell, didPressViewUserButon sender: Any) {
        let indexPath = self.tblBusinessOwners.indexPathForRow(at: view.center)!
        let userData = dataDict[indexPath.row]
        delegate?.businessOwnerViewDelegate(self, userData:userData,isDelete:0, didPressLoginButton:0)
    }
    
    func businessOwnerTableViewCellDelagate(view: BusinessOwnerTableViewCell, didPressEditUserButon sender: Any) {
        let indexPath = self.tblBusinessOwners.indexPathForRow(at: view.center)!
        let userData = dataDict[indexPath.row]
        delegate?.businessOwnerViewDelegate(self, userData:userData,isDelete:0, didPressLoginButton:1)
    }
    
    func businessOwnerTableViewCellDelagate(view: BusinessOwnerTableViewCell, didPressEditCompany sender: Any) {
        let indexPath = self.tblBusinessOwners.indexPathForRow(at: view.center)!
        let userData = dataDict[indexPath.row]
     
        delegate?.businessOwnerViewDelegate(self, userData:userData,isDelete:0, didPressLoginButton:2)
    }
    
    func businessOwnerTableViewCellDelagate(view: BusinessOwnerTableViewCell, didPressDeleteUserButton sender: Any) {
        let indexPath = self.tblBusinessOwners.indexPathForRow(at: view.center)!
        let userData = dataDict[indexPath.row]
        delegate?.businessOwnerViewDelegate(self, userData:userData,isDelete:indexPath.row, didPressLoginButton:3)
    }
    
    func businessOwnerTableViewCellDelagate(view: BusinessOwnerTableViewCell, didPressSelectButton sender: Any) {
        let indexPath = self.tblBusinessOwners.indexPathForRow(at: view.center)!
        let userData = dataDict[indexPath.row]
        
        let myString = userData.user_id
        // let myInt = Int(myString)
        
        if arrSelectedRows.contains(myString){
            if let index = arrSelectedRows.index(of:myString) {
                arrSelectedRows.remove(at: index)
            }
        }
        else{
            arrSelectedRows.append(myString)
        }
        
        tblBusinessOwners.reloadRows(at: tblBusinessOwners!.indexPathsForVisibleRows!, with: .none)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView,
                                  willDecelerate decelerate: Bool)
    {
        //Bottom Refresh
        if scrollView == tblBusinessOwners{
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isNewDataLoading{
                    isNewDataLoading = true
                    if isRecordExist == 1{
                          self.actIndView.isHidden = false
                        
                        self.delegate?.businessOwnerViewDelegate(self, setNewOffset: self.newOffset)
                    }
                }
            }
        }
    }
}
