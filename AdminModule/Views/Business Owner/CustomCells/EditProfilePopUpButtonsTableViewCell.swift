//
//  EditProfilePopUpButtonsTableViewCell.swift
//  ROOFMATE
//
//  Created by OSX on 07/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit

protocol EditProfilePopUpButtonsTableViewCellDelegate:class{
    func editProfilePopUpButtonsTableViewCellDelegate(_ view:EditProfilePopUpButtonsTableViewCell, didPressUpdateButton sender:Any)
       func editProfilePopUpButtonsTableViewCellDelegate(_ view:EditProfilePopUpButtonsTableViewCell, didPressCancelButton sender:Any)
}

class EditProfilePopUpButtonsTableViewCell: UITableViewCell {
    
 @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
     weak var delegate:EditProfilePopUpButtonsTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        btnUpdate.layer.cornerRadius = 10
        btnUpdate.layer.masksToBounds = true
        btnCancel.layer.cornerRadius = 10
        btnCancel.layer.masksToBounds = true
       

        // Initialization code
    }

    @IBAction func btnUpdateAction(_ sender: Any) {
        self.delegate?.editProfilePopUpButtonsTableViewCellDelegate(self, didPressUpdateButton: sender)
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        self.delegate?.editProfilePopUpButtonsTableViewCellDelegate(self, didPressCancelButton: sender)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension EditProfilePopUpButtonsTableViewCell:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true);
        return false;
    }
}
