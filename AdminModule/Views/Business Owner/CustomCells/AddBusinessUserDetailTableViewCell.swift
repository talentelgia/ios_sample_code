//
//  EditUserDetailTableViewCell.swift
//  ROOFMATE
//
//  Created by OSX on 05/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit

class AddBusinessUserDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtValue: UITextField!
    @IBOutlet var downArrowImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       // txtValue.useUnderline()
        self.layoutIfNeeded()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func updateValues(data:[String:Any]){
        
        lblTitle.text = data["title"] as? String
        txtValue.text = data["value"] as? String
        txtValue.placeholder = data["title"] as? String

    }
}
