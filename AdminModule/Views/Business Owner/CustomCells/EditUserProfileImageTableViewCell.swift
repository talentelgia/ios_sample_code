//
//  EditUserProfileImageTableViewCell.swift
//  ROOFMATE
//
//  Created by OSX on 05/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit
protocol EditUserProfileImageTableViewCellDelegate:class {
     func editUserProfileImageTableViewCellDelegate(_ view:EditUserProfileImageTableViewCell)
}
class EditUserProfileImageTableViewCell: UITableViewCell {

    @IBOutlet weak var imgProfile: UIImageView!
    weak var delegate:EditUserProfileImageTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        imgProfile.layer.cornerRadius = 43
         imgProfile.layer.masksToBounds = true
        // Initialization code
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imgProfile.isUserInteractionEnabled = true
        imgProfile.addGestureRecognizer(tapGestureRecognizer)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func updateValues(data:UserModel){
        let str =  String(format:"%@%@",KImageBaseUrl,data.imageName as CVarArg)
        print(str)
        let url = URL(string: str)
        let image = UIImage(named: "no_Image")
        imgProfile.kf.setImage(with: url, placeholder: image)
   
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        // let tappedImage = tapGestureRecognizer.view as! UIImageView
        self.delegate?.editUserProfileImageTableViewCellDelegate(self)
    }
}
