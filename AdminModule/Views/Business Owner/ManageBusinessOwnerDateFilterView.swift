//
//  ManageBusinessOwnerDateFilterView.swift
//  ROOFMATE
//
//  Created by OSX on 12/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit
//MARK:- Custom Protocols
protocol ManageBusinessOwnerDateFilterViewDelgate:class {
    func manageBusinessOwnerDateFilterViewDelgate(_ view:ManageBusinessOwnerDateFilterView,isStartTxtFld:Bool)
    func manageBusinessOwnerDateFilterViewDelgate(_ view:ManageBusinessOwnerDateFilterView,didPressCloseBtn sender:Any)
    func manageBusinessOwnerDateFilterViewDelgate(_ view:ManageBusinessOwnerDateFilterView,didPressResetBtn sender:Any)
    func manageBusinessOwnerDateFilterViewDelgate(_ view:ManageBusinessOwnerDateFilterView,didPressSearchBtn sender:Any)
    
}
//MARK:- View Life Cycle
class ManageBusinessOwnerDateFilterView: UIView {
    
    
    
    //MARK:-IBOutlets
    
    @IBOutlet var startTxtFld: UITextField!
    
    @IBOutlet var endtxtFld: UITextField!
    
    @IBOutlet var bgndImgView: UIImageView!
    
    weak var delegate:ManageBusinessOwnerDateFilterViewDelgate?
    var isStart:Bool = false
    var datePicker = UIDatePicker()
    
}
//MARK:- Custom Methods
extension ManageBusinessOwnerDateFilterView{
    func updateUI(imgIcon:UIImage){
        bgndImgView.image = imgIcon
    }
    func setUPDatepicker(){
        datePicker.datePickerMode = .date
    }
}

//MARK:- UIButton Actions
extension ManageBusinessOwnerDateFilterView{
    @IBAction func onResetBtnTapped(_ sender: Any) {
        delegate?.manageBusinessOwnerDateFilterViewDelgate(self, didPressResetBtn: sender)
    }
    
    @IBAction func onSearchBtnTapped(_ sender: Any) {
        delegate?.manageBusinessOwnerDateFilterViewDelgate(self, didPressSearchBtn: sender)
    }
    
    @IBAction func onCloseBtnTapped(_ sender: Any) {
        delegate?.manageBusinessOwnerDateFilterViewDelgate(self, didPressCloseBtn: sender)
    }
}
//MARK:- UITextFieldDelegate
extension ManageBusinessOwnerDateFilterView:UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.startTxtFld{
            isStart = true
            self.startTxtFld.inputView = datePicker
           delegate?.manageBusinessOwnerDateFilterViewDelgate(self, isStartTxtFld: true)
        }else {
            self.endtxtFld.inputView = datePicker
            
            delegate?.manageBusinessOwnerDateFilterViewDelgate(self, isStartTxtFld: false)
            
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case self.startTxtFld:
            self.isStart = false
        default:
            break
        }
    }
    
}
