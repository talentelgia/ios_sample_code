//
//  BusinessOwnerChangeView.swift
//  ROOFMATE
//
//  Created by OSX on 08/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import UIKit
protocol BusinessOwnerChangeViewDelegate:class{
    func businessOwnerChangeViewDelegate(_ view:BusinessOwnerChangeView, didPressCrossButton sender:Any)
     func businessOwnerChangeViewDelegate(_ view:BusinessOwnerChangeView, didPressRadiButton sender:Int)
    
}
class BusinessOwnerChangeView: UIView {
    
    //MARK:-IBOutlets
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var viewPopUp: UIView!
    @IBOutlet weak var btnActive: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnInactive: UIButton!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var delegate:BusinessOwnerChangeViewDelegate?
    var selectedRadioButtonValue :Int = 1
    
    func updateUI(imgIcon:UIImage){
        imgBG.image = imgIcon
    }
    func setUpUI() {
        viewPopUp.layer.cornerRadius = 10
        viewPopUp.layer.masksToBounds = true
    
        btnUpdate.layer.cornerRadius = 5
        btnUpdate.layer.masksToBounds = true
        btnCancel.layer.cornerRadius = 5
        btnCancel.layer.masksToBounds = true
    }
    
}

//MARK:- UiButton Actions
extension BusinessOwnerChangeView{
    
    //MARK:-UIButton Action Methods.
   
    @IBAction func btnActiveAction(_ sender: UIButton) {
        if self.btnActive.currentImage == #imageLiteral(resourceName: "inactive") {
            self.btnActive.setImage(#imageLiteral(resourceName: "active"), for: .normal)
            self.btnDelete.setImage(#imageLiteral(resourceName: "inactive"), for: .normal)
            self.btnInactive.setImage(#imageLiteral(resourceName: "inactive"), for: .normal)
             selectedRadioButtonValue = 1
        }else {
             selectedRadioButtonValue = 0
        }
    }
    
    @IBAction func btnInActiveAction(_ sender: UIButton) {
     
        if self.btnInactive.currentImage == #imageLiteral(resourceName: "inactive") {
            self.btnInactive.setImage(#imageLiteral(resourceName: "active"), for: .normal)
            self.btnDelete.setImage(#imageLiteral(resourceName: "inactive"), for: .normal)
            self.btnActive.setImage(#imageLiteral(resourceName: "inactive"), for: .normal)
            selectedRadioButtonValue = 2
        }else {
            selectedRadioButtonValue = 0
        }
    }
    
    @IBAction func btnDeleteAction(_ sender: UIButton) {
        if self.btnDelete.currentImage == #imageLiteral(resourceName: "inactive") {
            self.btnDelete.setImage(#imageLiteral(resourceName: "active"), for: .normal)
            self.btnInactive.setImage(#imageLiteral(resourceName: "inactive"), for: .normal)
            self.btnActive.setImage(#imageLiteral(resourceName: "inactive"), for: .normal)
            selectedRadioButtonValue = 3
        }else {
            selectedRadioButtonValue = 0
        }
    }
    
    @IBAction func btnCrossAction(_ sender: Any) {
        self.delegate?.businessOwnerChangeViewDelegate(self, didPressCrossButton: sender)
    }
    @IBAction func btnCancelAction(_ sender: Any) {
           self.delegate?.businessOwnerChangeViewDelegate(self, didPressCrossButton: sender)
    }
    
    @IBAction func btnUpdateAction(_ sender: Any) {
        self.delegate?.businessOwnerChangeViewDelegate(self, didPressRadiButton: selectedRadioButtonValue)
    }

}

//MARK:- Custom methods
extension BusinessOwnerChangeView{
    /*
     Active = 1
     In-active = 2
     Delete = 3
   */
   
}
