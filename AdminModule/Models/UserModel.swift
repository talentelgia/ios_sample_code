//
//  UserModel.swift
//  Crown Gospel
//
//  Created by mac min  on 09/01/18.
//  Copyright © 2018 mac min . All rights reserved.
//

import Foundation
class UserModel: NSObject {
    
    // Variables
    
    var imageName = ""
    var first_name = ""
    var last_name = ""
    var phone_number = ""
    var token = ""
    var email = ""
    var user_id = ""
    var user_type = ""
    var status = ""
    var slug = ""
    var companySlug = ""
    var validity = ""
    var validityInterval = ""
    var planPurchaseDate = ""
    var plan_id = ""
    var company : CompanyModel = CompanyModel()
    var plan : PlanModel = PlanModel()
    var address : AddressModel = AddressModel()
    var user_Role : UserRoleModel = UserRoleModel()
    var password =  ""
    var imgBase64 = ""
    var card = ""
    var qbo_company_id = ""
    var company_id = ""
   
}


