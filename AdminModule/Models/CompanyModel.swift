//
//  CompanyModel.swift
//  ROOFMATE
//
//  Created by OSX on 02/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import Foundation
class CompanyModel: NSObject {
    
    // Variables
    var comName = ""
    var compId = ""
    var logo = ""
    var stripe_private_key = ""
    var stripe_public_key = ""
    var qbo_company_id = ""
    var qbo_client_id = ""
    var qbo_client_secret = ""
}
