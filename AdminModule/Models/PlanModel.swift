//
//  PlanModel.swift
//  ROOFMATE
//
//  Created by OSX on 02/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//


import Foundation
class PlanModel: NSObject {
    
    // Variables
    var planName = ""
    var planId = ""
    var createdAt = ""
    var updatedAt = ""
    var currency = ""
    var planDescription = ""
    var validity = ""
    var validityInterval = ""
    var status = ""
    var slug = ""
    var price = ""
}

