//
//  AddressModel.swift
//  ROOFMATE
//
//  Created by OSX on 05/02/18.
//  Copyright © 2018 Talentelgia. All rights reserved.
//

import Foundation
class AddressModel: NSObject {
    
    // Variables
    
    var address_one = ""
    var address_two = ""
    var city = ""
    var created_at = ""
    var id = ""
    var link = ""
    var postal_code = ""
    var state = ""
    var user_id = ""
}
